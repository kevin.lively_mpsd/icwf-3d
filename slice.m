
sliceRedV1xyScan = zeros(eDimx*eDimy,eDimz);
tmp1 = reshape(abs(eigV(:,1)).^2, eDimx, eDimy, eDimz);
sliceRedV2xyScan = zeros(eDimx*eDimy,eDimz);
tmp2 = reshape(abs(eigV(:,2)).^2, eDimx, eDimy, eDimz);
sliceRedV3xyScan = zeros(eDimx*eDimy,eDimz);
tmp3 = reshape(abs(eigV(:,3)).^2, eDimx, eDimy, eDimz);
sliceRedV4xyScan = zeros(eDimx*eDimy,eDimz);
tmp4 = reshape(abs(eigV(:,4)).^2, eDimx, eDimy, eDimz);
sliceRedV5xyScan = zeros(eDimx*eDimy,eDimz);
tmp5 = reshape(abs(eigV(:,5)).^2, eDimx, eDimy, eDimz);

for i=1:eDimz
    tmp = tmp1(:,:,i);
    sliceRedV1xyScan(:,i)=tmp(:);
    tmp = tmp2(:,:,i);
    sliceRedV2xyScan(:,i)=tmp(:);
    tmp = tmp3(:,:,i);
    sliceRedV3xyScan(:,i)=tmp(:);
    tmp = tmp4(:,:,i);
    sliceRedV4xyScan(:,i)=tmp(:);
    tmp = tmp5(:,:,i);
    sliceRedV5xyScan(:,i)=tmp(:);
end

save('sliceRedV1xyScan.txt','sliceRedV1xyScan','-ascii','-double')
save('sliceRedV2xyScan.txt','sliceRedV2xyScan','-ascii','-double')
save('sliceRedV3xyScan.txt','sliceRedV3xyScan','-ascii','-double')
save('sliceRedV4xyScan.txt','sliceRedV4xyScan','-ascii','-double')
save('sliceRedV5xyScan.txt','sliceRedV5xyScan','-ascii','-double')
