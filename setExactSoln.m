
fprintf('Solving Exact\n')

H = Te + spdiags(sum(ePots,2),0,eDim,eDim);

ex=5;
[eigV, eigE] = eigs(H, ex, 'sa');
eigE = diag(eigE);

redVx = zeros(eDimx,ex+1);redVx(:,1) = eAxisx;
redVxy = zeros(eDimx*eDimy,ex);
sliceRedVxy = zeros(eDimx*eDimy,ex);
redVy = zeros(eDimy,ex+1);redVy(:,1) = eAxisy;
redVz = zeros(eDimz,ex+1);redVz(:,1) = eAxisz;
eSlicex = zeros(eDimx,ex+1);eSlicex(:,1) = eAxisx;
eSlicey = zeros(eDimy,ex+1);eSlicey(:,1) = eAxisy;
eSlicez = zeros(eDimz,ex+1);eSlicez(:,1) = eAxisz;
for i=1:ex
    eigV(:,i) = eigV(:,i)./sqrt(eigV(:,i)'*eigV(:,i)*de);
    tmp = reshape(abs(eigV(:,i)).^2,eDimx,eDimy,eDimz);
    redVx(:,i+1) = sum(sum(tmp,3),2)*dxe*dxe;
    redVy(:,i+1) = squeeze(sum(sum(tmp,3),1))*dxe*dxe;
    redVz(:,i+1) = squeeze(sum(sum(tmp,2),1))*dxe*dxe;
    
    tmp2 = sum(tmp,3)*dxe;
    redVxy(:,i) = tmp2(:);
    tmp3 = tmp(:,:,floor(eDimz/2));
    sliceRedVxy(:,i) = tmp3(:);

    tmp = reshape(eigV(:,i),eDimx,eDimy,eDimz);
    eSlicex(:,i +1) = squeeze(tmp(:,floor(eDimy/2),floor(eDimz/2)));
    eSlicey(:,i+1) = squeeze(tmp(floor(eDimx/2),:,floor(eDimz/2)));
    eSlicez(:,i+1) = squeeze(tmp(floor(eDimx/2),floor(eDimy/2),:));
end

save('eSlicex.txt', 'eSlicex','-ascii','-double');
save('eSlicey.txt', 'eSlicey','-ascii','-double');
save('eSlicez.txt', 'eSlicez','-ascii','-double');

save('eigV.txt', 'eigV','-ascii','-double');
save('eigE.txt', 'eigE','-ascii','-double');


save('redVx.txt','redVx','-ascii','-double');
save('redVxy.txt','redVxy','-ascii','-double');
save('sliceRedVxy.txt','sliceRedVxy','-ascii','-double');
save('redVy.txt','redVy','-ascii','-double');
save('redVz.txt','redVz','-ascii','-double');


if(ABC==true)
qfunc1 = zeros(eDim,2); 
qfunc2 = zeros(eDim,2); 
qfunc3 = zeros(eDim,2); 
tmp=reshape(q,eDimx,eDimy,eDimz);
ind=1;
for i=1:eDimx
    for j=1:eDimy
        for k=1:eDimz
            qfunc3(ind, 1) = sqrt(i^2 + j^2 + k^2);
            qfunc3(ind, 2) = tmp(i,j,k);
        end
    end
end

save('qfunc1.txt', 'qfunc1','-ascii','-double');
save('qfunc2.txt', 'qfunc2','-ascii','-double');
save('qfunc3.txt', 'qfunc3','-ascii','-double');
end
