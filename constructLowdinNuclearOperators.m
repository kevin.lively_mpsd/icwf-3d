Ne=200;Nn=1;
mi=1;rng(mi);
%Ne = 1;
%M=3;
nPeriods = 5;
I = 5e14; %W/cm^2

newCWF=false;
if(newCWF==false)
    saveCWFs=true;
end
lowdinDrivenBoot
re = zeros(eDim,3);
boundaryIndex = [];
for zi=1:eDimz
    for yi=1:eDimy
        m = (1:eDimz) + eDimy*(yi-1 + eDimz*(zi-1)); %flattened index
        re(m,1) = eAxisx(:);
        re(m,2) = eAxisy(yi);
        re(m,3) = eAxisz(zi);
        if(yi==1 || yi==eDimy || zi==1 || zi==eDimz)
            boundaryIndex = [boundaryIndex m];
        else
            boundaryIndex = [boundaryIndex m(1) m(end)];
        end
        %g(m,1) = gAxis(:); 
        %g(m,2) = gAxis(yi); 
        %g(m,3) = gAxis(zi); 
    end
end
Ven = zeros(nDim,eDim);
for Ri=1:nDim
    Ven(Ri,:) = Z(1)./sqrt( (re(:,1) - (m1/(m1+m2))*nAxis(Ri)).^2 ...
          +          (re(:,2)).^2 ...
          +          (re(:,3)).^2 ...
          +          softEN   )   ...
          +        Z(1)./sqrt( (re(:,1) + (m2/(m1+m2))*nAxis(Ri)).^2 ...
          +          (re(:,2)).^2 ...
          +          (re(:,3)).^2 ...
          +          softEN   );
end

phi_e = load(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/BOCan.mat'));
phi_e = phi_e.phiCan;

if(Ne>size(phi_e,2))
    Ne = size(phi_e,2);
else
    phi_e = phi_e(:,1:Ne);
end

Ne = size(phi_e,2);
Neflat = [];
for i=1:Ne
    for k=i:Ne
        Neflat = [Neflat i + Ne*(k-1) ];
    end
end


NucOp = zeros(nDim,max(size(Neflat)));
for Ef=1:max(size(Neflat))
    [iek, iel] = ind2sub([Ne,Ne],Neflat(Ef));
    tmp = phi_e(:,iek).*phi_e(:,iel); 
    NucOp(:,Ef) = Ven*tmp;
    fprintf(' %f percent done\n',100*Ef/max(size(Neflat)))
    %if(mod(Ef,100)==0)
    %    save(strcat(savePath,'NucOp.txt'),'NucOp','-ascii','-double')
    %end
    
end

save(strcat(savePath,'NucOp.txt'),'NucOp','-ascii','-double')
