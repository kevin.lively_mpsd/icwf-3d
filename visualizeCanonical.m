
if(false)
BOgsCanFlat = reshape(BOgsCan,eDimx,eDimy,eDimz,size(BOgsCan,2));
BOexCanFlat = reshape(BOexCan,eDimx,eDimy,eDimz,size(BOexCan,2));


tmpGs = zeros(eDimx,size(BOgsCan,2)+1); tmpGs(:,1) = eAxisx;
tmpEs = zeros(eDimx,size(BOexCan,2)+1); tmpEx(:,1) = eAxisx;
for i=1:size(BOgsCan,2)
    tmpGs(:,i+1) = squeeze(BOgsCanFlat(:,floor(eDimy/2),floor(eDimz/2),i));
end
for i=1:size(BOexCan,2)
    tmpEx(:,i+1) = squeeze(BOexCanFlat(:,floor(eDimy/2),floor(eDimz/2),i));
end
save('tmpGs.txt','tmpGs','-ascii','-double');
save('tmpEx.txt','tmpEx','-ascii','-double');
end %DEBUG

BOCanFlat = reshape(phiCan,eDimx,eDimy,eDimz,size(phiCan,2));


tmp = zeros(eDimx,size(phiCan,2)+1); tmp(:,1) = eAxisx;
for i=1:size(phiCan,2)
    tmp(:,i+1) = squeeze(BOCanFlat(:,floor(eDimy/2),floor(eDimz/2),i));
end
save('tmpCan.txt','tmp','-ascii','-double');
