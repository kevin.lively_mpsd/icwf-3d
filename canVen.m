%Simulation box length
boxL = 20; uniformBox=true;
a = 2*boxL; dxe = 0.3;
boxLn = 20;

%nuclear and electronic grid spacing
dxn = 0.05;

%electron grid volume element
de = 1;
for ei=1:3
    de = de*dxe;
end

%Charge of ions as seen from electrons
Z = [-1];
m1 = 1836; m2 =1836; mu_n = m1*m2/(m1+m2); 

mu_e = 2*m1/(2*m1 + 1);

%CWF softening parameters,
softEN = 0.1;



%x, y and z axes and dimensions. 
if(uniformBox == true)
    eBoxLx = -boxL; eBoxRx = boxL; eAxisx = (eBoxLx:dxe:eBoxRx).'; eDimx = max(size(eAxisx));
    eBoxLy = -boxL; eBoxRy = boxL; eAxisy = (eBoxLy:dxe:eBoxRy).'; eDimy = max(size(eAxisy));
    eBoxLz = -boxL; eBoxRz = boxL; eAxisz = (eBoxLz:dxe:eBoxRz).'; eDimz = max(size(eAxisz));

    eAxis = [eAxisx, eAxisy, eAxisz];

    nBoxL = 0.5; nBoxR = boxLn; nAxis = (nBoxL:dxn:nBoxR).'; nDim = max(size(nAxis));
end

%Total number of grid points
eDim = eDimx*eDimy*eDimz;

%Box parameters, may be useful later
eBoxL = [eBoxLx, eBoxLy, eBoxLz]; 
eBoxR = [eBoxRx, eBoxRy, eBoxRz]; 
eBoxDims = [eDimx, eDimy, eDimz];

nBoxL = [nBoxL]; 
nBoxR = [nBoxR]; 
nBoxDims = [nDim];

loadUniformOp

[U,Sig,V] = svd(Ven,'econ');
save('VenNuc.txt','U','-ascii','-double');
save('VenEle.mat','V','-v7.3');
Sig=diag(Sig);
save('VenSig.txt','Sig','-ascii','-double');
