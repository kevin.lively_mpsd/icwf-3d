
NTraj=10000;M=5

mi=1;rng(mi);
nPeriods = 5;
I = 5e13; %W/cm^2
drivenBoot

%Sample from gaussians
dxeS = dxe/50;
dyeS = dxe/50;
dzeS = dxe/50;
sampleEAxisx = (eBoxLx:(dxeS):eBoxRx).';
sampleEAxisy = (eBoxLy:(dyeS):eBoxRy).';
sampleEAxisz = (eBoxLz:(dzeS):eBoxRz).';

redEx = load(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(0.1),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/redEx.txt'));
redEy = load(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(0.1),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/redEy.txt'));
redEz = load(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(0.1),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/redEz.txt'));

redExSpline = spline(eAxisx,redEx, sampleEAxisx);
redEySpline = spline(eAxisy,redEy, sampleEAxisy);
redEzSpline = spline(eAxisz,redEz, sampleEAxisz);

%phi = kron(gausX,kron(gausY,gausZ)); phi=phi./sqrt(phi'*phi*de);
%PD = reshape(abs(phi).^2,max(size(sampleEAxisx)),max(size(sampleEAxisy)),max(size(sampleEAxisz)));
%PDaux = PD;

%mesh and positions are  Nc x 3 cartesian directions
mesh_e1 = zeros(NTraj,3);

re1 = zeros(NTraj,3);

%[mesh_e1(:,1),mesh_e1(:,2),mesh_e1(:,3)] = mcsampling(PDaux,NTraj,1e-12);
%[mesh_e2(:,1),mesh_e2(:,2),mesh_e2(:,3)] = mcsampling(PDaux,NTraj,1e-12);
cgx = cumsum(redExSpline*dxeS);
cgy = cumsum(redEySpline*dyeS);
cgz = cumsum(redEzSpline*dzeS);
for Ni=1:NTraj
    [dum,mesh_e1(Ni,1)] = min(abs(rand(1) - cgx));
    [dum,mesh_e1(Ni,2)] = min(abs(rand(1) - cgy));
    [dum,mesh_e1(Ni,3)] = min(abs(rand(1) - cgz));
end



r = rand(NTraj,1);
re1(:,1) = (0.5-r)*dxeS + sampleEAxisx(mesh_e1(:,1));
r = rand(NTraj,1);
re1(:,2) = (0.5-r)*dyeS + sampleEAxisy(mesh_e1(:,2));
r = rand(NTraj,1);
re1(:,3) = (0.5-r)*dzeS + sampleEAxisz(mesh_e1(:,3));
        
valsDiag = [-2*ones(nDim,1)];
vals = [ones(nDim,1)];
nLapl = spdiags(valsDiag,0,nDim,nDim) ...
      + spdiags(vals, 1, nDim,nDim)     + spdiags(vals, -1, nDim,nDim);

nLapl = (1/dxn^2)*nLapl; 
Tn = (-1/(2*mu_n))*nLapl;

for Ni=1:NTraj
       Vn_ea = Z(1)./sqrt( (re1(Ni,1) - (m1/(m1+m2))*nAxis).^2 ...
              +          (re1(Ni,2)).^2 ...
              +          (re1(Ni,3)).^2  + softEN) ...
              +      Z(1)./sqrt( (re1(Ni,1) + (m2/(m1+m2))*nAxis).^2 ...
              +          (re1(Ni,2)).^2 ...
              +          (re1(Ni,3)).^2 + softEN);
    [eigVN,eigN] = eigs(Tn + spdiags(Vn_ea + 1./nAxis,0,nDim,nDim),M,'sa');
    for Mi=1:M
        phi_n(:, Mi + M*(Ni-1)) = eigVN(:,Mi)./sqrt(eigVN(:,Mi)'*eigVN(:,Mi)*dxn);
        phiEnergyN(Mi+M*(Ni-1)) = eigN(Mi,Mi);
    end
    fprintf('%d percent done\n', 100*Ni/NTraj)
end

tmp = [nAxis, phi_n];
save(strcat(savePath,'phiNDist',savePathEnd,'.txt'),'tmp','-ascii','-double');
tmp = [nAxis, mean(abs(phi_n),2)];
save(strcat(savePath,'phiNAbsAvg',savePathEnd,'.txt'),'tmp','-ascii','-double');
save(strcat(savePath,'phiNEDist',savePathEnd,'.txt'),'phiEnergyN','-ascii','-double');


re1_ini=re1;
pe1 = zeros(size(re1));
pe1Half = zeros(size(re1));
currMax=0;
for ti=1:floor((tau+dt)/dt)
    pe1Half(:,2) = pe1(:,2) - 0.5*Efield(ti)*dt;
    re1(:,2) = re1(:,2) + pe1Half(:,2)*dt;
    pe1(:,2) = pe1Half(:,2) - 0.5*Efield(ti+1)*dt;

    if(max(abs(re1(:,2)-re1_ini(:,2)))> currMax)
        currMax = max(abs(re1(:,2)-re1_ini(:,2)));
        re1Max = re1;
    end
end

re1=re1Max;

valsDiag = [-2*ones(nDim,1)];
vals = [ones(nDim,1)];
nLapl = spdiags(valsDiag,0,nDim,nDim) ...
      + spdiags(vals, 1, nDim,nDim)     + spdiags(vals, -1, nDim,nDim);

nLapl = (1/dxn^2)*nLapl; 
Tn = (-1/(2*mu_n))*nLapl;

for Ni=1:NTraj
       Vn_ea = Z(1)./sqrt( (re1(Ni,1) - (m1/(m1+m2))*nAxis).^2 ...
              +          (re1(Ni,2)).^2 ...
              +          (re1(Ni,3)).^2  + softEN) ...
              +      Z(1)./sqrt( (re1(Ni,1) + (m2/(m1+m2))*nAxis).^2 ...
              +          (re1(Ni,2)).^2 ...
              +          (re1(Ni,3)).^2 + softEN);
    [eigVN,eigN] = eigs(Tn + spdiags(Vn_ea + 1./nAxis,0,nDim,nDim),M,'sa');
    for Mi=1:M
        phi_n(:, Mi + M*(Ni-1)) = eigVN(:,Mi)./sqrt(eigVN(:,Mi)'*eigVN(:,Mi)*dxn);
        phiEnergyN(Mi+M*(Ni-1)) = eigN(Mi,Mi);
    end
    fprintf('%d percent done\n', 100*Ni/NTraj)
end

tmp = [nAxis, phi_n];
save(strcat(savePath,'phiNMaxDist',savePathEnd,'.txt'),'tmp','-ascii','-double');
tmp = [nAxis, mean(abs(phi_n),2)];
save(strcat(savePath,'phiNMaxAbsAvg',savePathEnd,'.txt'),'tmp','-ascii','-double');
save(strcat(savePath,'phiNMaxEDist',savePathEnd,'.txt'),'phiEnergyN','-ascii','-double');
