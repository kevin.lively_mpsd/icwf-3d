params

re1 = load('re1.txt');
re2 = load('re2.txt');
rn = load('rn.txt');

%phi_e1 = load('phi_e1Debug'); phi_e1 = phi_e1.phi_e1;
%phi_e2 = load('phi_e2Debug'); phi_e2 = phi_e2.phi_e2;
%phi_n = load('phi_nDebug'); phi_n = phi_n.phi_n;

tmp = conj(phi_e2(:,1)).*phi_e2(:,1);
V = cgs(eLapl,-4*pi*tmp,1e-6,1000,[],[],rinv.*tmp);

MVee = (conj(phi_e1(:,1)).*phi_e1(:,1)).'*V*de; 

%Fix boundary condition of tmp such that potentail at boundary is q/r
q = sum(tmp*de);
bc = (-q/(4*pi))*eLapl(boundaryIndex,boundaryIndex)*rinv(boundaryIndex);
tmp2 = tmp; tmp2(boundaryIndex) = 0*tmp(boundaryIndex);

V2 = cgs(eLapl,-4*pi*tmp2,1e-6,1000,[],[],rinv.*tmp2);

MVee2 = (conj(phi_e1(:,1)).*phi_e1(:,1)).'*V2*de; 



if(false)
MVee = zeros(NTraj*M);
for k = 1:NTraj*M
    for j=k:NTraj*M

        tmp = conj(phi_e2(:,k)).*phi_e2(:,j);
        V = cgs(eLapl,-4*pi*tmp,1e-6,1000,[],[],-1*VeExt.*tmp);

        MVee(k,j) = (conj(phi_e1(:,k)).*phi_e1(:,j)).'*V*de; 
        fprintf('Finished alpha: %d beta: %d\n',k,j) 
        
    end
end
end
