

saveStates = true;

%Number of electrons and nuclei
Nele = 2;
Nnuc = 1; %H2 molecule, single vibrational degree of freedom

%Simulation box length
boxL = 25; uniformBox=true;
a = 2*boxL; dxe = 0.3;
boxLn = 20;

%nuclear and electronic grid spacing
dxn = 0.05;

%electron grid volume element
de = 1;
for ei=1:3
    de = de*dxe;
end

ex= 2;

%Charge of ions
Z = [-1];
m1 = 1836; m2 =1836; mu_n = m1*m2/(m1+m2); 

mu_e = 2*m1/(2*m1 + 1);

%CWF softening parameters,
softEN = 0.1;


%x, y and z axes and dimensions. 
if(uniformBox == true)
    eBoxLx = -boxL; eBoxRx = boxL; eAxisx = (eBoxLx:dxe:eBoxRx).'; eDimx = max(size(eAxisx));
    eBoxLy = -boxL; eBoxRy = boxL; eAxisy = (eBoxLy:dxe:eBoxRy).'; eDimy = max(size(eAxisy));
    eBoxLz = -boxL; eBoxRz = boxL; eAxisz = (eBoxLz:dxe:eBoxRz).'; eDimz = max(size(eAxisz));

    eAxis = [eAxisx, eAxisy, eAxisz];

    nBoxL = 0.2; nBoxR = boxLn; nAxis = (nBoxL:dxn:nBoxR).'; nDim = max(size(nAxis));
end

%Total number of grid points
eDim = eDimx*eDimy*eDimz;

%Box parameters, may be useful later
eBoxL = [eBoxLx, eBoxLy, eBoxLz]; 
eBoxR = [eBoxRx, eBoxRy, eBoxRz]; 
eBoxDims = [eDimx, eDimy, eDimz];

nBoxL = [nBoxL]; 
nBoxR = [nBoxR]; 
nBoxDims = [nDim];

savePath = strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_nBoxL_',num2str(nBoxL),'_softEN_',num2str(softEN),'/');

mkdir(savePath);
init=true;

%pinv tol
tolerance=1e-8;
