
%% K1  
[C_dot_1, flag] = gmres(S,-1j*(H)*C,[],tolerance,100,L,U);
%[C_dot_1, flag] = minres(S,-1j*(H + Efield(t)*dOp1)*C,tolerance,100);

%% k2
C_aux = C + C_dot_1*dt/2;
[C_dot_2, flag] = gmres(S,-1j*(H)*C_aux,[],tolerance,100,L,U);
%[C_dot_2, flag] = minres(S,-1j*(H+Eaux*dOp1)*C_aux,tolerance,100);

%% k3
C_aux = C + C_dot_2*dt/2;
[C_dot_3, flag] = gmres(S,-1j*(H)*C_aux,[],tolerance,100,L,U);
%[C_dot_3, flag] = minres(S,-1j*(H+Eaux*dOp1)*C_aux,tolerance,100);

%% k4
C_aux = C + C_dot_3*dt;
[C_dot_4, flag] = gmres(S,-1j*(H)*C_aux,[],tolerance,100,L,U);
%[C_dot_4, flag] = minres(S,-1j*(H + Efield(t+1)*dOp1)*C_aux,tolerance,100);

%% EVOLVED CONDITIONAL WAVEFUNCTION AND TRAJECTORIES
C = C + (dt/6)*(C_dot_1 + 2*C_dot_2 + 2*C_dot_3 + C_dot_4);
