
fprintf('Propagating Uniform Sampling\n')

Sinv = pinv(S,tolerance);
if(GPU==true)
    Sinv = gpuArray(Sinv);
    HGPU = gpuArray(H);
    C_ini = gpuArray(C_ini);
    dOp1GPU = gpuArray(dOp1);
end


Is = [1e13 5e13 1e14 5e14];
%Is = [1e14];
periods = [0.5 1 2 5 10];
%periods = [5];
for Ii = 1:max(size(Is))
    I = Is(Ii);
    for periodIndex = 1:max(size(periods))
        nPeriods = periods(periodIndex); 
        tau = nPeriods*(2*pi/oml); 

        A0 = sqrt(2*I/3.50944758e16)/oml;
        Agauge = A0*cos((pi/(tau))*(time-tau/2)).^2 .* cos(oml*(time-tau/2) + pi/2);
        Agauge(time>tau)=0;
        Efield = gradient(Agauge,dt);
        
        fileNameEnd=strcat('_dt_',num2str(dt),'_I_',num2str(log10(I)),'_tau_',num2str(tau),'_oml_',num2str(oml),'_dir_',num2str(1),'_'); %Pumpdir fixed to 1

        a = dir(strcat(savePath,'RPos',fileNameEnd));
        if(min(size(a))==0) %if this does not exist
            laserPropagate
        else
            tmp = load(strcat(savePath,'RPos',fileNameEnd));
            if(tmp(end) == 0) %Then this one didn't finish
                laserPropagate
            else
                continue %go on to the next laser parameters
            end
        end        
    end %periods 
end %intensity
