
fprintf('Sampling Trajectories\n')

%Sample from gaussians
dxeS = dxe/10;
dyeS = dxe/10;
dzeS = dxe/10;
sampleEAxisx = (eBoxLx:(dxeS):eBoxRx).';
sampleEAxisy = (eBoxLy:(dyeS):eBoxRy).';
sampleEAxisz = (eBoxLz:(dzeS):eBoxRz).';
ExactInterR = 2.75;
gausX = exp(-(sampleEAxisx-ExactInterR/2).^2) + exp(-(sampleEAxisx + ExactInterR/2).^2); gausX = gausX./sqrt(gausX'*gausX*dxeS);
gausY = exp(-(sampleEAxisy).^2); gausY = gausY./sqrt(gausY'*gausY*dxeS);
gausZ = exp(-(sampleEAxisz).^2); gausZ = gausZ./sqrt(gausZ'*gausZ*dxeS);
%phi = kron(gausX,kron(gausY,gausZ)); phi=phi./sqrt(phi'*phi*de);
%PD = reshape(abs(phi).^2,max(size(sampleEAxisx)),max(size(sampleEAxisy)),max(size(sampleEAxisz)));
%PDaux = PD;

%mesh and positions are  Nc x 3 cartesian directions
mesh_e1 = zeros(NTraj,3);

re1 = zeros(NTraj,3);

%[mesh_e1(:,1),mesh_e1(:,2),mesh_e1(:,3)] = mcsampling(PDaux,NTraj,1e-12);
%[mesh_e2(:,1),mesh_e2(:,2),mesh_e2(:,3)] = mcsampling(PDaux,NTraj,1e-12);
cgx = cumsum(abs(gausX).^2)*dxeS;
cgy = cumsum(abs(gausY).^2)*dyeS;
cgz = cumsum(abs(gausZ).^2)*dzeS;
for Ni=1:NTraj
    [dum,mesh_e1(Ni,1)] = min(abs(rand(1) - cgx));
    [dum,mesh_e1(Ni,2)] = min(abs(rand(1) - cgy));
    [dum,mesh_e1(Ni,3)] = min(abs(rand(1) - cgz));
end



r = rand(NTraj,1);
re1(:,1) = (0.5-r)*dxeS + sampleEAxisx(mesh_e1(:,1));
r = rand(NTraj,1);
re1(:,2) = (0.5-r)*dyeS + sampleEAxisy(mesh_e1(:,2));
r = rand(NTraj,1);
re1(:,3) = (0.5-r)*dzeS + sampleEAxisz(mesh_e1(:,3));

%Save initial positions
save(strcat(savePath,'re1',savePathEnd,'.txt'),'re1','-ascii','-double');
if(Nnuc>0)
    chi0 = load('chi0.txt');
    
    chi0Spline = spline(chi0(:,1),chi0(:,2),nAxis);
    chi0Spline = chi0Spline./sqrt(chi0Spline'*chi0Spline*dxn);
    %Sample from gaussians
    redN = zeros(size(chi0Spline));
    redN(chi0Spline>0) = chi0Spline(chi0Spline>0);
    redN = redN./sqrt(redN'*redN*dxn);
    PD = cumsum(abs(redN).^2*dxn);
   
    mesh_n = zeros(NTraj,1);
    for Ni=1:NTraj
        [dum,mesh_n(Ni)] = min(abs(rand(1) - PD));
    end
    r = rand(NTraj,1);
    rn = (0.5-r)*dxn + nAxis(mesh_n);
 
    save(strcat(savePath,'rn',savePathEnd,'.txt'),'rn','-ascii','-double');
end
