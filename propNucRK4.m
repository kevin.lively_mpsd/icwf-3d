chiOrig = chi;

chi1 = -1j*H*chi;
chi = chiOrig + chi1*dt/2;

chi2 = -1j*H*chi;
chi = chiOrig + chi2*dt/2;

chi3 = -1j*H*chi;
chi = chiOrig + chi3*dt;

chi4 = -1j*H*chi;

chi = chiOrig + (dt/6)*(chi1 + 2*chi2 + 2*chi3 + chi4);

