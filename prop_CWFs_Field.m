


dphi_e1_1 = -1j*(Te*phi_e1 + Ve_na.*phi_e1 - Efield(t)*rPump.*phi_e1);
phi_e1_aux = phi_e1 + dphi_e1_1*dt/2;

Eaux = 0.5*(Efield(t) + Efield(t+1));
dphi_e1_2 = -1j*(Te*phi_e1_aux + Ve_na.*phi_e1_aux - Eaux*rPump.*phi_e1_aux);
phi_e1_aux = phi_e1 + dphi_e1_2*dt/2;

dphi_e1_3 = -1j*(Te*phi_e1_aux + Ve_na.*phi_e1_aux - Eaux*rPump.*phi_e1_aux);
phi_e1_aux = phi_e1 + dphi_e1_3*dt;

dphi_e1_4 = -1j*(Te*phi_e1_aux + Ve_na.*phi_e1_aux - Efield(t+1)*rPump.*phi_e1_aux);

phi_e1 = phi_e1 + (dt/6)*(dphi_e1_1 + 2*dphi_e1_2 + 2*dphi_e1_3 + dphi_e1_4);


