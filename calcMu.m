
savePath = 'BO_dxe_0.3_boxL_15_dx_n_0.1_boxLn_20_softEN_0.1/';
BOs = load(strcat(savePath,'BOstates.m'));
BOs = BOs.BOs;

Mu1 = zeros(size(BOs,1),size(BOs,3),size(BOs,3));
Mu2 = zeros(size(BOs,1),size(BOs,3),size(BOs,3));
Mu3 = zeros(size(BOs,1),size(BOs,3),size(BOs,3));
for Ri=1:size(BOs,1)
    Mu1(Ri,:,:) = -squeeze(BOs(Ri,:,:))'*(repmat(re(:,1),1,size(BOs,3)).*squeeze(BOs(Ri,:,:)))*de;
    Mu2(Ri,:,:) = -squeeze(BOs(Ri,:,:))'*(repmat(re(:,2),1,size(BOs,3)).*squeeze(BOs(Ri,:,:)))*de;
    Mu3(Ri,:,:) = -squeeze(BOs(Ri,:,:))'*(repmat(re(:,3),1,size(BOs,3)).*squeeze(BOs(Ri,:,:)))*de;
end

tmp = Mu1(:);
save(strcat(savePath,'Mu1.txt'),'tmp','-ascii','-double');
tmp = Mu2(:);
save(strcat(savePath,'Mu2.txt'),'tmp','-ascii','-double');
tmp = Mu3(:);
save(strcat(savePath,'Mu3.txt'),'tmp','-ascii','-double');
