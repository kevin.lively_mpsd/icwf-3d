
re = zeros(eDim,3);
boundaryIndex = [];
for zi=1:eDimz
    for yi=1:eDimy
        m = (1:eDimz) + eDimy*(yi-1 + eDimz*(zi-1)); %flattened index
        re(m,1) = eAxisx(:);
        re(m,2) = eAxisy(yi);
        re(m,3) = eAxisz(zi);
        if(yi==1 || yi==eDimy || zi==1 || zi==eDimz)
            boundaryIndex = [boundaryIndex m];
        else
            boundaryIndex = [boundaryIndex m(1) m(end)];
        end
        %g(m,1) = gAxis(:); 
        %g(m,2) = gAxis(yi); 
        %g(m,3) = gAxis(zi); 
    end
end
Ven = zeros(nDim,eDim);
for Ri=1:nDim
    Ven(Ri,:) = Z(1)./sqrt( (re(:,1) - (m1/(m1+m2))*nAxis(Ri)).^2 ...
          +          (re(:,2)).^2 ...
          +          (re(:,3)).^2 ...
          +          softEN   )   ...
          +        Z(1)./sqrt( (re(:,1) + (m2/(m1+m2))*nAxis(Ri)).^2 ...
          +          (re(:,2)).^2 ...
          +          (re(:,3)).^2 ...
          +          softEN   );
end
valsDiag = [-6*ones(eDim,1)];
vals = [ones(eDim,1)];

%Operators are flattened with
%   x axis being the fastest changing index
%   y axis being the second fastest changing index
%   z axis being the slowest changing index 
eLapl = spdiags(valsDiag,0,eDim,eDim) ...
      + spdiags(vals, 1, eDim,eDim)     + spdiags(vals, -1, eDim,eDim) ...
      + spdiags(vals, eDimy, eDim,eDim)    + spdiags(vals, -eDimy, eDim,eDim) ...
      + spdiags(vals, eDimy*eDimz, eDim,eDim) + spdiags(vals, -eDimy*eDimz, eDim,eDim);
eLapl = (1/dxe^2)*eLapl; 
Te = (-1/(2*mu_e))*eLapl;
    
valsDiag = [-2*ones(nDim,1)];
vals = [ones(nDim,1)];
nLapl = spdiags(valsDiag,0,nDim,nDim) ...
      + spdiags(vals, 1, nDim,nDim)     + spdiags(vals, -1, nDim,nDim);

nLapl = (1/dxn^2)*nLapl; 
Tn = (-1/(2*mu_n))*nLapl;
