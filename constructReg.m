
%gsInd = 1:2*Ne*M:2*Ne*M*NRs;
%exInd = 2:2*Ne*M:2*Ne*M*NRs;


%BOGs = squeeze(BOstates(:,:,1)).';
%BOEx = squeeze(BOstates(:,:,2)).';
if(false)

BOEx = load(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/BOexRectified.mat'));
BOEx = (BOEx.tmp).';
BOGs = load(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/BOgsRectified.mat'));
BOGs = (BOGs.tmp).';

end %DEBUG

[DeltaBO,SigmaBO,UBO] = svd([BOGs, BOEx],'econ');
save(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/BODelta.mat'),'DeltaBO','-v7.3');
save(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/BOSigma.mat'),'SigmaBO','-v7.3');
save(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/BOU.mat'),'UBO','-v7.3');

%[Delta,Sigma,U] = svd(squeeze(BOstates(:,:,1)),'econ');



keep = [];
tol=1e-6;
for i=1:min(size(SigmaBO))
    if(SigmaBO(i,i)>tol)
        keep = [keep i];
    end
end
ACan = UBO(:,keep)*diag(1./sqrt(diag(SigmaBO(keep,keep))));

phiCan = [BOGs, BOEx]*ACan;
for i=1:max(size(keep))
    phiCan(:,i) = phiCan(:,i)./sqrt(phiCan(:,i)'*phiCan(:,i)*de);
end

save(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/BOCan.mat'),'phiCan','-v7.3');

%[DeltaBOEx,SigmaBOEx,UBOEx] = svd(BOEx,'econ');
%save(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/BOexDelta.mat'),'DeltaBOEx','-v7.3');
%save(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/BOexSigma.mat'),'SigmaBOEx','-v7.3');
%save(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/BOexU.mat'),'UBOEx','-v7.3');

%keep = [];
%tol=1e-6;
%for i=1:min(size(SigmaBOEx))
%    if(SigmaBOEx(i,i)>tol)
%        keep = [keep i];
%    end
%end
%ACan = UBOEx(:,keep)*diag(1./sqrt(diag(SigmaBOEx(keep,keep))));
%phiCan = BOEx*ACan;
%for i=1:max(size(keep))
%    phiCan(:,i) = phiCan(:,i)./sqrt(phiCan(:,i)'*phiCan(:,i)*de);
%end

%save(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/BOexCan.mat'),'phiCan','-v7.3');
