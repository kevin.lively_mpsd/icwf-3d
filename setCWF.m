
fprintf('Setting CWFs\n')

%CWFs are flattened by M, and Nc as
%   M being the fastest changing index
%   Nc being the slowest changing index

phi_e1 = zeros(eDim,NTraj*M);
if(Nnuc>0)
    phi_n = zeros(nDim,NTraj*M);
end

phiEnergy1 = zeros(NTraj*M,1);
phiEnergyN = zeros(NTraj*M,1);
for Ni=1:NTraj
    [eigV1,eigE1] = eigs(Te + spdiags(Ve_na(:,Ni),0,eDim,eDim),M,'sa');
    if(Nnuc>0)
        [eigVN,eigN] = eigs(Tn + spdiags(Vn_ea(:,Ni) + Vnn,0,nDim,nDim),M,'sa');
    end
    for Mi=1:M
        phi_e1(:, Mi + M*(Ni-1)) = eigV1(:,Mi)./sqrt(eigV1(:,Mi)'*eigV1(:,Mi)*de);
        if(Nnuc>0)
            phi_n(:, Mi + M*(Ni-1)) = eigVN(:,Mi)./sqrt(eigVN(:,Mi)'*eigVN(:,Mi)*dxn);
            phiEnergyN(Mi+M*(Ni-1)) = eigN(Mi,Mi);
        end
        phiEnergy1(Mi+M*(Ni-1)) = eigE1(Mi,Mi);
    end
    fprintf('%d percent done\n', 100*Ni/NTraj)
end
phi_e1_ini = phi_e1;
phi_n_ini = phi_n;

if(saveCWFs)
    save(strcat(savePath,'phi_e1',savePathEnd,'.mat'),'phi_e1') 
    aux = [nAxis, phi_n];
    save(strcat(savePath,'phi_n',savePathEnd,'.txt'),'aux', '-ascii','-double')
    save(strcat(savePath,'phiEnergy1',savePathEnd,'.txt'),'phiEnergy1','-ascii','-double') 
end

