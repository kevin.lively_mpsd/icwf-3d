

fprintf('Sampling Trajectories\n')

%Sample from gaussians
dxeS = dxe/50;
dyeS = dxe/50;
dzeS = dxe/50;
sampleEAxisx = (eBoxLx:(dxeS):eBoxRx).';
sampleEAxisy = (eBoxLy:(dyeS):eBoxRy).';
sampleEAxisz = (eBoxLz:(dzeS):eBoxRz).';

redEx = load(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(0.1),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/redEx.txt'));
redEy = load(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(0.1),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/redEy.txt'));
redEz = load(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(0.1),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/redEz.txt'));

redExSpline = spline(eAxisx,redEx, sampleEAxisx);
redEySpline = spline(eAxisy,redEy, sampleEAxisy);
redEzSpline = spline(eAxisz,redEz, sampleEAxisz);

%phi = kron(gausX,kron(gausY,gausZ)); phi=phi./sqrt(phi'*phi*de);
%PD = reshape(abs(phi).^2,max(size(sampleEAxisx)),max(size(sampleEAxisy)),max(size(sampleEAxisz)));
%PDaux = PD;

%mesh and positions are  Nc x 3 cartesian directions
mesh_e1 = zeros(NTraj,3);

re1 = zeros(NTraj,3);

%[mesh_e1(:,1),mesh_e1(:,2),mesh_e1(:,3)] = mcsampling(PDaux,NTraj,1e-12);
%[mesh_e2(:,1),mesh_e2(:,2),mesh_e2(:,3)] = mcsampling(PDaux,NTraj,1e-12);
cgx = cumsum(redExSpline*dxeS);
cgy = cumsum(redEySpline*dyeS);
cgz = cumsum(redEzSpline*dzeS);
for Ni=1:NTraj
    [dum,mesh_e1(Ni,1)] = min(abs(rand(1) - cgx));
    [dum,mesh_e1(Ni,2)] = min(abs(rand(1) - cgy));
    [dum,mesh_e1(Ni,3)] = min(abs(rand(1) - cgz));
end



r = rand(NTraj,1);
re1(:,1) = (0.5-r)*dxeS + sampleEAxisx(mesh_e1(:,1));
r = rand(NTraj,1);
re1(:,2) = (0.5-r)*dyeS + sampleEAxisy(mesh_e1(:,2));
r = rand(NTraj,1);
re1(:,3) = (0.5-r)*dzeS + sampleEAxisz(mesh_e1(:,3));

%Save initial positions
save(strcat(savePath,'re1',savePathEnd,'.txt'),'re1','-ascii','-double');
if(Nnuc>0)
    chi0 = load(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(0.1),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/chi0.txt'));
    redN = abs(chi0).^2;
    
    dxnS = 0.1/50;
    sampleNAxis = (nBoxL:dxnS:nBoxR).';
    tmp = (0.5:0.1:boxLn).';
    redNSpline = spline(tmp,redN,sampleNAxis);
    %Sample from gaussians
    PD = cumsum(redNSpline*dxnS);
   
    mesh_n = zeros(NTraj,1);
    for Ni=1:NTraj
        [dum,mesh_n(Ni)] = min(abs(rand(1) - PD));
    end
    r = rand(NTraj,1);
    rn = (0.5-r)*dxnS + sampleNAxis(mesh_n);
 
    save(strcat(savePath,'rn',savePathEnd,'.txt'),'rn','-ascii','-double');
end
