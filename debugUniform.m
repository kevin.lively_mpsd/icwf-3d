
NRs = max(size((0.5:0.05:20).'));
Rflats = [];
for Rk=1:NRs
    for Rl=Rk:NRs
        Rflats = [Rflats Rk + NRs*(Rl-1)];
    end
end

[Rk,Rl] = ind2sub([NRs,NRs],Rflats(NRs))
