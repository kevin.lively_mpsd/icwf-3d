
if(true)
mi=1;rng(mi);
%Ne = 1;
%M=3;
nPeriods = 5;
I = 5e13; %W/cm^2

newCWF=false;
if(newCWF==false)
    saveCWFs=true;
end
Ne=1;Nn=1;
lowdinDrivenBoot
loadUniformOp
BOs = load(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/BOstates.mat'));
BOstates = BOs.BOstates;
BOenergy = load(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/BOenergy.txt'));


BOgs = squeeze(BOstates(:,:,1));
BOex = squeeze(BOstates(:,:,2));

BOgs = BOgs.';
BOex = BOex.';
MU01_1 = zeros(size(BOgs,2),1);
MU01_2 = zeros(size(BOgs,2),1);
MU01_3 = zeros(size(BOgs,2),1);

MU00_1 = zeros(size(BOgs,2),1);
MU00_2 = zeros(size(BOgs,2),1);
MU00_3 = zeros(size(BOgs,2),1);

MU11_1 = zeros(size(BOgs,2),1);
MU11_2 = zeros(size(BOgs,2),1);
MU11_3 = zeros(size(BOgs,2),1);

for Ri=1:size(BOgs,2)
    MU01_1(Ri) = BOgs(:,Ri)'*(re(:,1).*BOex(:,Ri))*de;
    MU01_2(Ri) = BOgs(:,Ri)'*(re(:,2).*BOex(:,Ri))*de;
    MU01_3(Ri) = BOgs(:,Ri)'*(re(:,3).*BOex(:,Ri))*de;
    
    MU00_1(Ri) = BOgs(:,Ri)'*(re(:,1).*BOgs(:,Ri))*de;
    MU00_2(Ri) = BOgs(:,Ri)'*(re(:,2).*BOgs(:,Ri))*de;
    MU00_3(Ri) = BOgs(:,Ri)'*(re(:,3).*BOgs(:,Ri))*de;
    
    MU11_1(Ri) = BOex(:,Ri)'*(re(:,1).*BOex(:,Ri))*de;
    MU11_2(Ri) = BOex(:,Ri)'*(re(:,2).*BOex(:,Ri))*de;
    MU11_3(Ri) = BOex(:,Ri)'*(re(:,3).*BOex(:,Ri))*de;

    fprintf(' %f percent done\n', 100*Ri/size(BOgs,2))
end

save(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/Mu01_1.txt'),'MU01_1','-ascii','-double');
save(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/Mu01_2.txt'),'MU01_2','-ascii','-double');
save(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/Mu01_3.txt'),'MU01_3','-ascii','-double');

save(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/Mu00_1.txt'),'MU00_1','-ascii','-double');
save(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/Mu00_2.txt'),'MU00_2','-ascii','-double');
save(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/Mu00_3.txt'),'MU00_3','-ascii','-double');


BOgsRec = zeros(size(BOgs));
BOexRec = zeros(size(BOex));

for Ri=1:size(BOgs,2)
    if(mean(BOgs(:,Ri))<0)
        BOgsRec(:,Ri) = -1*BOgs(:,Ri);
    else
        BOgsRec(:,Ri) = BOgs(:,Ri);
    end
    
    if(mean(BOex(:,Ri).*re(:,1))<0)
        BOexRec(:,Ri) = -1*BOex(:,Ri);
    else
        BOexRec(:,Ri) = BOex(:,Ri);
    end
end

for Ri=1:size(BOgs,2)
    MU01_1(Ri) = BOgsRec(:,Ri)'*(re(:,1).*BOexRec(:,Ri))*de;
    
    fprintf(' %f percent done\n', 100*Ri/size(BOgs,2))
end
tmp = [nAxis, MU01_1];
save('tmp.txt','tmp','-ascii','-double');
end %DEBUG
tmp = BOgsRec.';
save(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/BOgsRectified'),'tmp','-v7.3');
tmp = BOexRec.';
save(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/BOexRectified'),'tmp','-v7.3');
