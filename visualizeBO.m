
BOgsFlat = reshape(BOgs,NRs,eDimx,eDimy,eDimz);
BOexFlat = reshape(BOex,NRs,eDimx,eDimy,eDimz);

redEx = zeros(eDimx,NRs+1); redEx(:,1) = eAxisx;
redEy = zeros(eDimy,NRs+1); redEy(:,1) = eAxisy;
redEz = zeros(eDimz,NRs+1); redEz(:,1) = eAxisz;

redGx = zeros(eDimx,NRs+1); redGx(:,1) = eAxisx;
redGy = zeros(eDimy,NRs+1); redGy(:,1) = eAxisy;
redGz = zeros(eDimz,NRs+1); redGz(:,1) = eAxisz;

sliceEx = zeros(eDimx,NRs+1); sliceEx(:,1) = eAxisx;
sliceEy = zeros(eDimy,NRs+1); sliceEy(:,1) = eAxisy;
sliceEz = zeros(eDimz,NRs+1); sliceEz(:,1) = eAxisz;

sliceGx = zeros(eDimx,NRs+1); sliceGx(:,1) = eAxisx;
sliceGy = zeros(eDimy,NRs+1); sliceGy(:,1) = eAxisy;
sliceGz = zeros(eDimz,NRs+1); sliceGz(:,1) = eAxisz;

for Rk=1:NRs
    redEx(:,Rk+1) = squeeze(sum(sum(abs(BOexFlat(Rk,:,:,:)).^2,4),3))*dxe^2;
    redEy(:,Rk+1) = squeeze(sum(sum(abs(BOexFlat(Rk,:,:,:)).^2,4),2))*dxe^2;
    redEz(:,Rk+1) = squeeze(sum(sum(abs(BOexFlat(Rk,:,:,:)).^2,2),3))*dxe^2;
    
    redGx(:,Rk+1) = squeeze(sum(sum(abs(BOgsFlat(Rk,:,:,:)).^2,4),3))*dxe^2;
    redGy(:,Rk+1) = squeeze(sum(sum(abs(BOgsFlat(Rk,:,:,:)).^2,4),2))*dxe^2;
    redGz(:,Rk+1) = squeeze(sum(sum(abs(BOgsFlat(Rk,:,:,:)).^2,2),3))*dxe^2;
    
    sliceEx(:,Rk+1) = squeeze(BOexFlat(Rk,:,67,67));
    sliceEy(:,Rk+1) = squeeze(BOexFlat(Rk,67,:,67));
    sliceEz(:,Rk+1) = squeeze(BOexFlat(Rk,67,67,:));
    
    sliceGx(:,Rk+1) = squeeze(BOgsFlat(Rk,:,67,67));
    sliceGy(:,Rk+1) = squeeze(BOgsFlat(Rk,67,:,67));
    sliceGz(:,Rk+1) = squeeze(BOgsFlat(Rk,67,67,:));
end

save('BOgsRedx.txt','redGx','-ascii','-double')
save('BOgsRedy.txt','redGy','-ascii','-double')
save('BOgsRedz.txt','redGz','-ascii','-double')

save('BOexRedx.txt','redEx','-ascii','-double')
save('BOexRedy.txt','redEy','-ascii','-double')
save('BOexRedz.txt','redEz','-ascii','-double')

save('BOgsSlicex.txt','sliceGx','-ascii','-double')
save('BOgsSlicey.txt','sliceGy','-ascii','-double')
save('BOgsSlicez.txt','sliceGz','-ascii','-double')

save('BOexSlicex.txt','sliceEx','-ascii','-double')
save('BOexSlicey.txt','sliceEy','-ascii','-double')
save('BOexSlicez.txt','sliceEz','-ascii','-double')
