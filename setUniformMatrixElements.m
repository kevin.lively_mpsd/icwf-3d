

fprintf('Setting Matrix Elements\n')
%%Construct Hamiltonian matrix elements

Sn = phi_n'*phi_n*dxn;

S = Se1.*Sn;% + 2*real(Se1e2);
Sinv = pinv(S,tolerance);
%One Body Operators

%Kinetic
MTn = Se1.*(phi_n'*Tn*phi_n*dxn);


NRs = max(size(Rsample));
MVnn = Se1.*(phi_n'*(repmat(1./nAxis,1,2*Ne*M*NRs).*phi_n)*dxn);

rxTrajs = repmat(re(:,1).',NRs,1);
ryTrajs = repmat(re(:,2).',NRs,1);
rzTrajs = repmat(re(:,3).',NRs,1);
%BOex = squeeze(BOstates(:,:,2));

RTrajs = repmat(nAxis,1,2*Ne*M*NRs);
ROp = (Se1.*(phi_n'*(RTrajs.*phi_n)*dxn));

r1BO00 = (BOgs(1:NRs,:).*rxTrajs)*(BOgs(1:NRs,:))'*de;
r1BO01 = (BOgs(1:NRs,:).*rxTrajs)*(BOex(1:NRs,:))'*de;
r1BO11 = (BOex(1:NRs,:).*rxTrajs)*(BOex(1:NRs,:))'*de;

r2BO00 = (BOgs(1:NRs,:).*ryTrajs)*(BOgs(1:NRs,:))'*de;
r2BO01 = (BOgs(1:NRs,:).*ryTrajs)*(BOex(1:NRs,:))'*de;
r2BO11 = (BOex(1:NRs,:).*ryTrajs)*(BOex(1:NRs,:))'*de;

r3BO00 = (BOgs(1:NRs,:).*rzTrajs)*(BOgs(1:NRs,:))'*de;
r3BO01 = (BOgs(1:NRs,:).*rzTrajs)*(BOex(1:NRs,:))'*de;
r3BO11 = (BOex(1:NRs,:).*rzTrajs)*(BOex(1:NRs,:))'*de;

dOp1 =  ones(2*Ne*M*NRs);
dOp2 =  ones(2*Ne*M*NRs);
dOp3 =  ones(2*Ne*M*NRs);
%KERKernOp = zeros(nDim,2*Ne*M*NRs);
redNOp = zeros(nDim,2*Ne*M*NRs,2*Ne*M*NRs);
for j=1:2*Ne*M*NRs
    redNOp(:,j,:) = phi_n.*(conj(phi_n(:,j))*Se1(j,:));
    %for j=1:2*Ne*M*NRs
    %    redNOp(:,j,i) = conj(phi_n(:,j)).*phi_n(:,i)*Se1(j,i);
    %end
end

TBO00 = (BOgs(1:NRs,:)*Te)*(BOgs(1:NRs,:)')*de;
TBO01 = (BOgs(1:NRs,:)*Te)*(BOex(1:NRs,:)')*de;
TBO11 = (BOex(1:NRs,:)*Te)*(BOex(1:NRs,:)')*de;
MTe1 = ones(2*M*Ne*NRs);
MVen = ones(2*M*Ne*NRs);

phi_e1Alpha = zeros(eDim,2*Ne*M); %Column wise
phi_e1Beta  = zeros(eDim,2*Ne*M); %Row wise

gsInd = (1:Ne*M);
exInd = (Ne*M+1):2*Ne*M;
for Rk=1:NRs
    [dum, Ri] = min(abs(Rsample(Rk)-nAxis));
    for Rl = Rk:NRs
        [dum, Rj] = min(abs(Rsample(Rl)-nAxis));

        phi_e1Alpha(:,gsInd) = repmat(BOgs(Rj,:).',1,Ne*M);
        phi_e1Alpha(:,exInd) = repmat(BOex(Rj,:).',1,Ne*M);
        phi_e1Beta(:,gsInd) = repmat(BOgs(Ri,:).',1,Ne*M);
        phi_e1Beta(:,exInd) = repmat(BOex(Ri,:).',1,Ne*M);

        %Upper diag block, always
        indL = gsInd + 2*Ne*M*(Rk-1);
        indR = exInd + 2*Ne*M*(Rl-1);
        MTe1(indL,indR) = MTe1(indL,indR)*TBO01(Ri,Rj);
        dOp1(indL,indR) = dOp1(indL,indR)*r1BO01(Ri,Rj);
        dOp2(indL,indR) = dOp2(indL,indR)*r2BO01(Ri,Rj);
        dOp3(indL,indR) = dOp3(indL,indR)*r3BO01(Ri,Rj);
        for ki=1:Ne*M
            %phi_e1Beta index should match indL
            beta  = indL(ki) - 2*Ne*M*(Rk-1);
            %phi_e1Alpha index should match indR
            alphaInd = indR - 2*Ne*M*(Rl-1);
            MVen(indL(ki),indR) = MVen(indL(ki),indR).*sum((repmat(conj(phi_n(:,indL(ki))),1,Ne*M).*phi_n(:,indR)).*(Ven*(repmat(conj(phi_e1Beta(:,beta)),1,Ne*M).*phi_e1Alpha(:,alphaInd))),1)*dxn*de;
        end

        if(Ri~=Rj) %Do full blocks on upper triangular
            %Upper Left
            indL = gsInd + 2*Ne*M*(Rk-1);
            indR = gsInd + 2*Ne*M*(Rl-1);
            MTe1(indL,indR) = MTe1(indL,indR)*TBO00(Ri,Rj);
            dOp1(indL,indR) = dOp1(indL,indR)*r1BO00(Ri,Rj);
            dOp2(indL,indR) = dOp2(indL,indR)*r2BO00(Ri,Rj);
            dOp3(indL,indR) = dOp3(indL,indR)*r3BO00(Ri,Rj);
            for ki=1:Ne*M
                %phi_e1Beta index should match indL
                beta  = indL(ki) - 2*Ne*M*(Rk-1);
                %phi_e1Alpha index should match indR
                alphaInd = indR - 2*Ne*M*(Rl-1);
                MVen(indL(ki),indR) = MVen(indL(ki),indR).*sum((repmat(conj(phi_n(:,indL(ki))),1,Ne*M).*phi_n(:,indR)).*(Ven*(repmat(conj(phi_e1Beta(:,beta)),1,Ne*M).*phi_e1Alpha(:,alphaInd))),1)*dxn*de;
            end
                                              
            %Lower Left                       
            indL = exInd + 2*Ne*M*(Rk-1);     
            indR = gsInd + 2*Ne*M*(Rl-1);
            MTe1(indL,indR) = MTe1(indL,indR)*TBO01(Ri,Rj);
            dOp1(indL,indR) = dOp1(indL,indR)*r1BO01(Ri,Rj);
            dOp2(indL,indR) = dOp2(indL,indR)*r2BO01(Ri,Rj);
            dOp3(indL,indR) = dOp3(indL,indR)*r3BO01(Ri,Rj);
            for ki=1:Ne*M
                %phi_e1Beta index should match indL
                beta  = indL(ki) - 2*Ne*M*(Rk-1);
                %phi_e1Alpha index should match indR
                alphaInd = indR - 2*Ne*M*(Rl-1);
                MVen(indL(ki),indR) = MVen(indL(ki),indR).*sum((repmat(conj(phi_n(:,indL(ki))),1,Ne*M).*phi_n(:,indR)).*(Ven*(repmat(conj(phi_e1Beta(:,beta)),1,Ne*M).*phi_e1Alpha(:,alphaInd))),1)*dxn*de;
            end
            
            %Lower Right
            indL = exInd + 2*Ne*M*(Rk-1);
            indR = exInd + 2*Ne*M*(Rl-1);
            MTe1(indL,indR) = MTe1(indL,indR)*TBO11(Ri,Rj);
            dOp1(indL,indR) = dOp1(indL,indR)*r1BO11(Ri,Rj);
            dOp2(indL,indR) = dOp2(indL,indR)*r2BO11(Ri,Rj);
            dOp3(indL,indR) = dOp3(indL,indR)*r3BO11(Ri,Rj);
            for ki=1:Ne*M
                %phi_e1Beta index should match indL
                beta  = indL(ki) - 2*Ne*M*(Rk-1);
                %phi_e1Alpha index should match indR
                alphaInd = indR - 2*Ne*M*(Rl-1);
                MVen(indL(ki),indR) = MVen(indL(ki),indR).*sum((repmat(conj(phi_n(:,indL(ki))),1,Ne*M).*phi_n(:,indR)).*(Ven*(repmat(conj(phi_e1Beta(:,beta)),1,Ne*M).*phi_e1Alpha(:,alphaInd))),1)*dxn*de;
            end

        else
            for Nm=1:2*Ne*M
                for Nn=Nm:2*Ne*M
                    indL = Nm + 2*Ne*M*(Rk-1);
                    indR = Nn + 2*Ne*M*(Rl-1);
                    if(Nm<=Ne*M) % Upper Left
                        MTe1(indL,indR) = MTe1(indL,indR)*TBO00(Ri,Rj);
                        dOp1(indL,indR) = dOp1(indL,indR)*r1BO00(Ri,Rj);
                        dOp2(indL,indR) = dOp2(indL,indR)*r2BO00(Ri,Rj);
                        dOp3(indL,indR) = dOp3(indL,indR)*r3BO00(Ri,Rj);
                        MVen(indL,indR) = MVen(indL,indR).*sum((conj(phi_n(:,indL)).*phi_n(:,indR)).*(Ven*(conj(phi_e1Beta(:,Nm)).*phi_e1Alpha(:,Nn))),1)*dxn*de;
                    elseif(Nm>Ne*M) %Lower Right
                        MTe1(indL,indR) = MTe1(indL,indR)*TBO11(Ri,Rj);
                        dOp1(indL,indR) = dOp1(indL,indR)*r1BO11(Ri,Rj);
                        dOp2(indL,indR) = dOp2(indL,indR)*r2BO11(Ri,Rj);
                        dOp3(indL,indR) = dOp3(indL,indR)*r3BO11(Ri,Rj);
                        MVen(indL,indR) = MVen(indL,indR).*sum((conj(phi_n(:,indL)).*phi_n(:,indR)).*(Ven*(conj(phi_e1Beta(:,Nm)).*phi_e1Alpha(:,Nn))),1)*dxn*de;
                    end
                end
            end
        end
        fprintf('Rk=%d Rl=%d\n',Rk,Rl)
    end
    save(strcat(savePath,'MTe1',savePathEnd,'.txt'),'MTe1','-ascii','-double');
    save(strcat(savePath,'MVen',savePathEnd,'.txt'),'MVen','-ascii','-double');
    save(strcat(savePath,'dOp1',savePathEnd,'.txt'),'dOp1','-ascii','-double');
    save(strcat(savePath,'dOp2',savePathEnd,'.txt'),'dOp2','-ascii','-double');
    save(strcat(savePath,'dOp3',savePathEnd,'.txt'),'dOp3','-ascii','-double');
end



MTe1 = MTe1 - tril(MTe1) + triu(MTe1)';
MVen = MVen - tril(MVen) + triu(MVen)';
dOp1 = dOp1 - tril(dOp1) + triu(dOp1)';
dOp2 = dOp2 - tril(dOp2) + triu(dOp2)';
dOp3 = dOp3 - tril(dOp3) + triu(dOp3)';


dOp1 = -1*Sn.*dOp1;
dOp2 = -1*Sn.*dOp2;
dOp3 = -1*Sn.*dOp3;

MTe1 = Sn.*MTe1;

H =  MTn + MTe1 + MVen + MVnn;
U = MVen + MVnn;

save(strcat(savePath,'H',savePathEnd,'.txt'),'H','-ascii','-double');
save(strcat(savePath,'U',savePathEnd,'.txt'),'U','-ascii','-double');
save(strcat(savePath,'S',savePathEnd,'.txt'),'S','-ascii','-double');
save(strcat(savePath,'Se1',savePathEnd,'.txt'),'Se1','-ascii','-double');
save(strcat(savePath,'Sn',savePathEnd,'.txt'),'Sn','-ascii','-double');
save(strcat(savePath,'dOp1',savePathEnd,'.txt'),'dOp1','-ascii','-double');
save(strcat(savePath,'dOp2',savePathEnd,'.txt'),'dOp2','-ascii','-double');
save(strcat(savePath,'dOp3',savePathEnd,'.txt'),'dOp3','-ascii','-double');
save(strcat(savePath,'MTe1',savePathEnd,'.txt'),'MTe1','-ascii','-double');
save(strcat(savePath,'MVen',savePathEnd,'.txt'),'MVen','-ascii','-double');
