BOBoot


valsDiag = [-2*ones(nDim,1)];
vals = [ones(nDim,1)];
nLapl = spdiags(valsDiag,0,nDim,nDim) ...
      + spdiags(vals, 1, nDim,nDim)     + spdiags(vals, -1, nDim,nDim);

nLapl = (1/dxn^2)*nLapl; 
Tn = (-1/(2*mu_n))*nLapl;


BOPES = load(strcat(savePath,'BOenergy.txt'));
Mu1 = load(strcat(savePath,'Mu1.txt'));
Mu1 = reshape(Mu1,nDim,ex,ex);
Mu2 = load(strcat(savePath,'Mu2.txt'));
Mu2 = reshape(Mu2,nDim,ex,ex);
Mu3 = load(strcat(savePath,'Mu3.txt'));
Mu3 = reshape(Mu3,nDim,ex,ex);

[eigGs, Egs] = eigs(Tn + diag(BOPES(:,2)),1,'sa');
chi0 = eigGs./sqrt(eigGs'*eigGs*dxn);



dt = 0.1; dt_imag=0.01;
Tf = 1000; time = 0:dt:Tf;
saveInt = 0.1;
saveIntervalInd = floor(saveInt/dt);
saveTime = time(1:saveIntervalInd:end); saveInd = 1;

energy = 0:0.001:1;
a1 = zeros(max(size(energy)),ex);a1(:,1) = energy;
a2 = zeros(max(size(energy)),ex);a2(:,1) = energy;
a3 = zeros(max(size(energy)),ex);a3(:,1) = energy;

C1 = zeros(max(size(saveTime)),ex); C1(:,1) = saveTime;
C2 = zeros(max(size(saveTime)),ex); C2(:,1) = saveTime;
C3 = zeros(max(size(saveTime)),ex); C3(:,1) = saveTime;

RPos = zeros(max(size(saveTime)),ex); RPos(:,1) = saveTime;

for exi=2:ex
    saveInd=1;
    V = BOPES(:,exi+1);
    H = Tn + diag(V);
    
    chi=chi0;
    chi = chi./sqrt(chi'*chi*dxn);
    for ti=1:max(size(time))
        propNucRK4
        if(mod(time(ti),saveInt)==0)
            tmp = abs((Mu1(:,1,exi).*Mu1(:,exi,1))).^2.*chi;
            C1(saveInd,exi) = chi0'*tmp*dxn*exp(1j*Egs*time(ti));
            RPos(saveInd,exi) = chi'*(nAxis.*chi)*dxn;
            saveInd = saveInd + 1;
        end
    end
    a1(:,exi) = TCFSpectra(C1,saveTime,energy,exi);
    save(strcat(savePath,'Abs1.txt'),'a1','-ascii','-double');

    saveInd=1;
    chi= chi0;
    chi = chi./sqrt(chi'*chi*dxn);
    for ti=1:max(size(time))
        propNucRK4
        if(mod(time(ti),saveInt)==0)
            tmp = abs(Mu2(:,1,exi).*Mu2(:,exi,1)).^2.*chi;
            C2(saveInd,exi) = chi0'*tmp*dxn*exp(1j*Egs*time(ti));
            saveInd = saveInd + 1;
        end
    end
    a2(:,exi) = TCFSpectra(C2,saveTime,energy,exi);
    
    saveInd=1;
    chi= chi0;
    chi = chi./sqrt(chi'*chi*dxn);
    for ti=1:max(size(time))
        propNucRK4
        if(mod(time(ti),saveInt)==0)
            tmp = abs(Mu3(:,1,exi).*Mu3(:,exi,1)).^2.*chi;
            C3(saveInd,exi) = chi0'*tmp*dxn*exp(1j*Egs*time(ti));
            saveInd = saveInd + 1;
        end
    end
    a3(:,exi) = TCFSpectra(C3,saveTime,energy,exi);
end

save(strcat(savePath,'Abs1.txt'),'a1','-ascii','-double');
save(strcat(savePath,'Abs2.txt'),'a2','-ascii','-double');
save(strcat(savePath,'Abs3.txt'),'a3','-ascii','-double');

save(strcat(savePath,'RPos.txt'),'RPos','-ascii','-double');

tmp = real(C1);
save(strcat(savePath,'C1Re.txt'),'tmp','-ascii','-double');
tmp = real(C2);
save(strcat(savePath,'C2Re.txt'),'tmp','-ascii','-double');
tmp = real(C3);
save(strcat(savePath,'C3Re.txt'),'tmp','-ascii','-double');

tmp = imag(C1);
save(strcat(savePath,'C1Im.txt'),'tmp','-ascii','-double');
tmp = imag(C2);
save(strcat(savePath,'C2Im.txt'),'tmp','-ascii','-double');
tmp = imag(C3);
save(strcat(savePath,'C3Im.txt'),'tmp','-ascii','-double');
