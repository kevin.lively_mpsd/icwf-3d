pw = reshape(exp(-1j*100*r(:)),eDimx,eDimy,eDimz);
f1 = zeros(eDimx,2);
Tepw = reshape(Te*pw(:),eDimx,eDimy,eDimz);
f2 = zeros(eDimx,2);

ind=1;
for ind=1:eDimx
            r = sqrt(xeAxisx(ind).^2 + xeAxisy(ind).^2 + xeAxisz(ind).^2);
            f1(ind,1) = r 
            f2(ind,1) = r; 
        
            f1(ind,2) = exp(-1j*10*r);
            f2(ind,2) = Tepw(i,j,k);
end

aux = real(f1);
save('pwRe.txt','aux','-ascii','-double')
aux = imag(f1);
save('pwIm.txt','aux','-ascii','-double')
aux = real(f2);
save('TepwRe.txt','aux','-ascii','-double')
aux = imag(f2);
save('TepwIm.txt','aux','-ascii','-double')
