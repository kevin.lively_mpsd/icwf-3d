
redEVyz = zeros(eDimx,eDimy*eDimz);
redEVxz = zeros(eDimy,eDimx*eDimz);
redEVxy = zeros(eDimz,eDimx*eDimy);

redN = zeros(nDim,2); redN(:,1) = nAxis;
phi2 = zeros(eDim,1);
phi = phi_e1*C;
for i=1:NTraj*M
    phi2 = phi2 + C(i)*(conj(phi_e1).*repmat(phi_e1(:,i),1,NTraj*M))*(conj(C).*(Sn(:,i)));

    redN(:,2) = redN(:,2) + C(i)*((conj(phi_n).*repmat(phi_n(:,i),1,NTraj*M)))*(conj(C).*Se1(:,i));
end
flatPhi2 = reshape(phi2,eDimx,eDimy,eDimz);

for rei=1:eDimx
    redEVyz(rei,:) = reshape(squeeze(flatPhi2(rei,:,:)),1,eDimy*eDimz);
    redEVxz(rei,:) = reshape(squeeze(flatPhi2(:,rei,:)),1,eDimy*eDimz);
    redEVxy(rei,:) = reshape(squeeze(flatPhi2(:,:,rei)),1,eDimy*eDimz);
end

redEx = [eAxisx squeeze(sum(sum(flatPhi2,3),2))*dxe*dxe];
redEy = [eAxisx squeeze(sum(sum(flatPhi2,3),1)).'*dxe*dxe];
redEz = [eAxisx squeeze(sum(sum(flatPhi2,1),2))*dxe*dxe];
phiEx = [eAxisx squeeze(sum(sum(flatPhi,3),2))*dxe*dxe];
phiEy = [eAxisx squeeze(sum(sum(flatPhi,3),1)).'*dxe*dxe];
phiEz = [eAxisx squeeze(sum(sum(flatPhi,1),2))*dxe*dxe];
save('tmp/redEVyz.txt','redEVyz','-ascii','-double');
save('tmp/redEVxz.txt','redEVxz','-ascii','-double');
save('tmp/redEVxy.txt','redEVxy','-ascii','-double');
save('tmp/redEx.txt','redEx','-ascii','-double');
save('tmp/redEy.txt','redEy','-ascii','-double');
save('tmp/redEz.txt','redEz','-ascii','-double');
save('tmp/phiEx.txt','phiEx','-ascii','-double');
save('tmp/phiEy.txt','phiEy','-ascii','-double');
save('tmp/phiEz.txt','phiEz','-ascii','-double');
save('tmp/redN.txt','redN','-ascii','-double');
