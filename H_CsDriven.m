


rxTrajs = repmat(re(:,1),1,NTraj*M);
ryTrajs = repmat(re(:,2),1,NTraj*M);
rzTrajs = repmat(re(:,3),1,NTraj*M);
RTrajs = repmat(nAxis,1,NTraj*M);

Se1_ini = (phi_e1_ini'*phi_e1_ini*de);
Sn_ini = (phi_n_ini'*phi_n_ini*dxn);

Se1 = Se1_ini;
Sn = Sn_ini;

dOp1 =  -1*Sn.*(phi_e1'*(rxTrajs.*phi_e1)*de);
dOp2 =  -1*Sn.*(phi_e1'*(ryTrajs.*phi_e1)*de);
dOp3 =  -1*Sn.*(phi_e1'*(rzTrajs.*phi_e1)*de);

KERKernOp = zeros(nDim,NTraj*M);
%BOex = squeeze(BOstates(:,:,2));
%for i=1:NTraj*M
%    KERKernOp = KERKernOp + (BOex*phi_e1(:,i)).*phi_n(:,i);
%end

ROp = (Se1.*(phi_n'*(RTrajs.*phi_n)*dxn));



for pumpDir = 1:2
    COrig=C_ini;
    C = COrig; 
    
    propC = -1i*Sinv*H;
    if(pumpDir==1)
        propC2 = -1i*Sinv*dOp1;
    else
        propC2 = -1i*Sinv*dOp2;
    end
    
    saveInd=1; 
    saveIndNuc=1; 
    fileNameEnd=strcat('_tau_',num2str(tau),'_oml_',num2str(oml),'_dir_',num2str(pumpDir));

    fileNameD = strcat('dipole',fileNameEnd); 
    fileNameN = strcat('redN',  fileNameEnd); 
    fileNameR = strcat('RPos',  fileNameEnd); 
    %fileNameK = strcat('KERKernel',  fileNameEnd); 
    fileNameE = strcat('Energy',fileNameEnd); 
    dipole = zeros(max(size(saveTime)),4); dipole(:,1) = saveTime;
    RPos = zeros(max(size(saveTime)),1);
    redN = zeros(nDim,max(size(saveTimeNuc))+1); redN(:,1) = nAxis;
    KERKern = zeros(nDim,max(size(saveTimeNuc)));
    Energy = zeros(max(size(saveTime)),2); Energy(:,1) = saveTime;
    for t=1:max(size(time))
        if(mod(time(t),saveTime(saveInd))==0)
            %saveRDE
            dipole(saveInd,2) = gather(real(C'*dOp1*C));
            dipole(saveInd,3) = gather(real(C'*dOp2*C));
            dipole(saveInd,4) = gather(real(C'*dOp3*C));
            Energy(saveInd,2) = gather(real(C'*H*C));
            RPos(saveInd) = gather(real(C'*ROp*C));

            if(mod(time(t),saveTimeNuc(saveIndNuc))==0)
                %redE = zeros(eDimx,2);
                for i = 1:NTraj*M
                    redN(:,saveIndNuc+1) = redN(:,saveIndNuc+1) + C(i)*((conj(phi_n).*repmat(phi_n(:,i),1,NTraj*M)))*(conj(C).*Se1(:,i));
                 %red_e = red_e + C(i)*((conj(phi_e1).*repmat(phi_e1(:,i),1,NTraj*M)))*(conj(C).*Se2(:,i).*Sn(:,i));
                end
                %KERKern(:,saveIndNuc) = KERKernOp*C;
                redN = real(redN);
                saveIndNuc = saveIndNuc+1;
            end
            if(mod(saveTime(saveInd),100)==0)
                save(strcat(savePath,fileNameD,savePathEnd,'.txt'), 'dipole', '-ascii','-double')
                save(strcat(savePath,fileNameN,savePathEnd,'.txt'), 'redN', '-ascii','-double')
                %save(strcat(savePath,fileNameK,savePathEnd,'.txt'), 'KERKern', '-ascii','-double')
                save(strcat(savePath,fileNameR,savePathEnd,'.txt'), 'RPos', '-ascii','-double')
                save(strcat(savePath,fileNameE,savePathEnd,'.txt'), 'Energy', '-ascii','-double')
            end
            saveInd = saveInd + 1;
        end
        if(time(t)<= tau)
            runge_kutta_prop_FieldCs
        else
            runge_kutta_prop_Cs
        end
    end

    save(strcat(savePath,fileNameD,savePathEnd,'.txt'), 'dipole', '-ascii','-double')
    save(strcat(savePath,fileNameN,savePathEnd,'.txt'), 'redN', '-ascii','-double')
    %save(strcat(savePath,fileNameK,savePathEnd,'.txt'), 'KERKern', '-ascii','-double')
    save(strcat(savePath,fileNameR,savePathEnd,'.txt'), 'RPos', '-ascii','-double')
    save(strcat(savePath,fileNameE,savePathEnd,'.txt'), 'Energy', '-ascii','-double')
end


