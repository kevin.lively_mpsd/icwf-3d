

%FMT
%Rflats = []
%for Rk=1:NRs
%    for Rl=Rk:NRs
%        Rflats = [Rflats Rk + NRs*(Rl-1)];
%    end
%end
%
%[Rk,Rl] = ind2sub([NRs,NRs],Rflats(NRs+1));


fprintf('Setting Matrix Elements\n')

NRs = max(size(Rsample));
savePathEnd = strcat(savePathEnd,'_Rflat_',num2str(Rsubset(1)),'-',num2str(Rsubset(end)));
a = dir(strcat(savePath,'dOp3',savePathEnd,'.txt'));
if(min(size(a))>0) %if Restarting
    %load the other matricies
    MTe1 = load(strcat(savePath,'MTe1',savePathEnd,'.txt'));
    MVen = load(strcat(savePath,'MVen',savePathEnd,'.txt'));
    dOp1 = load(strcat(savePath,'dOp1',savePathEnd,'.txt'));
    dOp2 = load(strcat(savePath,'dOp2',savePathEnd,'.txt'));
    dOp3 = load(strcat(savePath,'dOp3',savePathEnd,'.txt'));
    for Rf=1:max(size(Rsubset))
        Rflat = Rsubset(Rf);
        [Rk,Rl] = ind2sub([NRs,NRs],Rflat);

        %Check if upper left entry in R block is 1
        i=1+2*Ne*M*(Rk-1);
        j=1+2*Ne*M*(Rl-1);
        %On first positive reading
        if(dOp3(i,j) == 1)
            RFlatStart=Rf;
            break
        end
    end      


else
    %initialize the matrices
    MTe1 = ones(2*M*Ne*NRs);
    MVen = ones(2*M*Ne*NRs);
    dOp1 =  ones(2*Ne*M*NRs);
    dOp2 =  ones(2*Ne*M*NRs);
    dOp3 =  ones(2*Ne*M*NRs);
    RFlatStart=1;
end




Sn = phi_n'*phi_n*dxn;

S = Se1.*Sn;% + 2*real(Se1e2);
Sinv = pinv(S,tolerance);
%One Body Operators

%Kinetic
MTn = Se1.*(phi_n'*Tn*phi_n*dxn);


NRs = max(size(Rsample));
MVnn = Se1.*(phi_n'*(repmat(1./nAxis,1,2*Ne*M*NRs).*phi_n)*dxn);

rxTrajs = repmat(re(:,1).',NRs,1);
ryTrajs = repmat(re(:,2).',NRs,1);
rzTrajs = repmat(re(:,3).',NRs,1);
%BOex = squeeze(BOstates(:,:,2));

RTrajs = repmat(nAxis,1,2*Ne*M*NRs);
ROp = (Se1.*(phi_n'*(RTrajs.*phi_n)*dxn));

r1BO00 = (BOgs(1:NRs,:).*rxTrajs)*(BOgs(1:NRs,:))'*de;
r1BO01 = (BOgs(1:NRs,:).*rxTrajs)*(BOex(1:NRs,:))'*de;
r1BO11 = (BOex(1:NRs,:).*rxTrajs)*(BOex(1:NRs,:))'*de;

r2BO00 = (BOgs(1:NRs,:).*ryTrajs)*(BOgs(1:NRs,:))'*de;
r2BO01 = (BOgs(1:NRs,:).*ryTrajs)*(BOex(1:NRs,:))'*de;
r2BO11 = (BOex(1:NRs,:).*ryTrajs)*(BOex(1:NRs,:))'*de;

r3BO00 = (BOgs(1:NRs,:).*rzTrajs)*(BOgs(1:NRs,:))'*de;
r3BO01 = (BOgs(1:NRs,:).*rzTrajs)*(BOex(1:NRs,:))'*de;
r3BO11 = (BOex(1:NRs,:).*rzTrajs)*(BOex(1:NRs,:))'*de;

TBO00 = (BOgs(1:NRs,:)*Te)*(BOgs(1:NRs,:)')*de;
TBO01 = (BOgs(1:NRs,:)*Te)*(BOex(1:NRs,:)')*de;
TBO11 = (BOex(1:NRs,:)*Te)*(BOex(1:NRs,:)')*de;

phi_e1Alpha = zeros(eDim,2*Ne*M); %Column wise
phi_e1Beta  = zeros(eDim,2*Ne*M); %Row wise

gsInd = (1:Ne*M);
exInd = (Ne*M+1):2*Ne*M;
for RflatInd=RFlatStart:max(size(Rsubset))
    Rflat = Rsubset(RflatInd);
    [Rk,Rl] = ind2sub([NRs, NRs],Rflat);
    [dum, Ri] = min(abs(Rsample(Rk)-nAxis));
    [dum, Rj] = min(abs(Rsample(Rl)-nAxis));
    
    phi_e1Alpha(:,gsInd) = repmat(BOgs(Rj,:).',1,Ne*M);
    phi_e1Alpha(:,exInd) = repmat(BOex(Rj,:).',1,Ne*M);
    phi_e1Beta(:,gsInd) = repmat(BOgs(Ri,:).',1,Ne*M);
    phi_e1Beta(:,exInd) = repmat(BOex(Ri,:).',1,Ne*M);

    %Upper diag block, always
    indL = gsInd + 2*Ne*M*(Rk-1);
    indR = exInd + 2*Ne*M*(Rl-1);
    MTe1(indL,indR) = MTe1(indL,indR)*TBO01(Ri,Rj);
    dOp1(indL,indR) = dOp1(indL,indR)*r1BO01(Ri,Rj);
    dOp2(indL,indR) = dOp2(indL,indR)*r2BO01(Ri,Rj);
    dOp3(indL,indR) = dOp3(indL,indR)*r3BO01(Ri,Rj);
    for ki=1:Ne*M
        %phi_e1Beta index should match indL
        beta  = indL(ki) - 2*Ne*M*(Rk-1);
        %phi_e1Alpha index should match indR
        alphaInd = indR - 2*Ne*M*(Rl-1);
        MVen(indL(ki),indR) = MVen(indL(ki),indR).*sum((repmat(conj(phi_n(:,indL(ki))),1,Ne*M).*phi_n(:,indR)).*(Ven*(repmat(conj(phi_e1Beta(:,beta)),1,Ne*M).*phi_e1Alpha(:,alphaInd))),1)*dxn*de;
    end

    if(Ri~=Rj) %Do full blocks on upper triangular
        %Upper Left
        indL = gsInd + 2*Ne*M*(Rk-1);
        indR = gsInd + 2*Ne*M*(Rl-1);
        MTe1(indL,indR) = MTe1(indL,indR)*TBO00(Ri,Rj);
        dOp1(indL,indR) = dOp1(indL,indR)*r1BO00(Ri,Rj);
        dOp2(indL,indR) = dOp2(indL,indR)*r2BO00(Ri,Rj);
        dOp3(indL,indR) = dOp3(indL,indR)*r3BO00(Ri,Rj);
        for ki=1:Ne*M
            %phi_e1Beta index should match indL
            beta  = indL(ki) - 2*Ne*M*(Rk-1);
            %phi_e1Alpha index should match indR
            alphaInd = indR - 2*Ne*M*(Rl-1);
            MVen(indL(ki),indR) = MVen(indL(ki),indR).*sum((repmat(conj(phi_n(:,indL(ki))),1,Ne*M).*phi_n(:,indR)).*(Ven*(repmat(conj(phi_e1Beta(:,beta)),1,Ne*M).*phi_e1Alpha(:,alphaInd))),1)*dxn*de;
        end
                                          
        %Lower Left                       
        indL = exInd + 2*Ne*M*(Rk-1);     
        indR = gsInd + 2*Ne*M*(Rl-1);
        MTe1(indL,indR) = MTe1(indL,indR)*TBO01(Ri,Rj);
        dOp1(indL,indR) = dOp1(indL,indR)*r1BO01(Ri,Rj);
        dOp2(indL,indR) = dOp2(indL,indR)*r2BO01(Ri,Rj);
        dOp3(indL,indR) = dOp3(indL,indR)*r3BO01(Ri,Rj);
        for ki=1:Ne*M
            %phi_e1Beta index should match indL
            beta  = indL(ki) - 2*Ne*M*(Rk-1);
            %phi_e1Alpha index should match indR
            alphaInd = indR - 2*Ne*M*(Rl-1);
            MVen(indL(ki),indR) = MVen(indL(ki),indR).*sum((repmat(conj(phi_n(:,indL(ki))),1,Ne*M).*phi_n(:,indR)).*(Ven*(repmat(conj(phi_e1Beta(:,beta)),1,Ne*M).*phi_e1Alpha(:,alphaInd))),1)*dxn*de;
        end
        
        %Lower Right
        indL = exInd + 2*Ne*M*(Rk-1);
        indR = exInd + 2*Ne*M*(Rl-1);
        MTe1(indL,indR) = MTe1(indL,indR)*TBO11(Ri,Rj);
        dOp1(indL,indR) = dOp1(indL,indR)*r1BO11(Ri,Rj);
        dOp2(indL,indR) = dOp2(indL,indR)*r2BO11(Ri,Rj);
        dOp3(indL,indR) = dOp3(indL,indR)*r3BO11(Ri,Rj);
        for ki=1:Ne*M
            %phi_e1Beta index should match indL
            beta  = indL(ki) - 2*Ne*M*(Rk-1);
            %phi_e1Alpha index should match indR
            alphaInd = indR - 2*Ne*M*(Rl-1);
            MVen(indL(ki),indR) = MVen(indL(ki),indR).*sum((repmat(conj(phi_n(:,indL(ki))),1,Ne*M).*phi_n(:,indR)).*(Ven*(repmat(conj(phi_e1Beta(:,beta)),1,Ne*M).*phi_e1Alpha(:,alphaInd))),1)*dxn*de;
        end

    else
        for Nm=1:2*Ne*M
            for Nn=Nm:2*Ne*M
                indL = Nm + 2*Ne*M*(Rk-1);
                indR = Nn + 2*Ne*M*(Rl-1);
                if(Nm<=Ne*M) % Upper Left
                    MTe1(indL,indR) = MTe1(indL,indR)*TBO00(Ri,Rj);
                    dOp1(indL,indR) = dOp1(indL,indR)*r1BO00(Ri,Rj);
                    dOp2(indL,indR) = dOp2(indL,indR)*r2BO00(Ri,Rj);
                    dOp3(indL,indR) = dOp3(indL,indR)*r3BO00(Ri,Rj);
                    MVen(indL,indR) = MVen(indL,indR).*sum((conj(phi_n(:,indL)).*phi_n(:,indR)).*(Ven*(conj(phi_e1Beta(:,Nm)).*phi_e1Alpha(:,Nn))),1)*dxn*de;
                elseif(Nm>Ne*M) %Lower Right
                    MTe1(indL,indR) = MTe1(indL,indR)*TBO11(Ri,Rj);
                    dOp1(indL,indR) = dOp1(indL,indR)*r1BO11(Ri,Rj);
                    dOp2(indL,indR) = dOp2(indL,indR)*r2BO11(Ri,Rj);
                    dOp3(indL,indR) = dOp3(indL,indR)*r3BO11(Ri,Rj);
                    MVen(indL,indR) = MVen(indL,indR).*sum((conj(phi_n(:,indL)).*phi_n(:,indR)).*(Ven*(conj(phi_e1Beta(:,Nm)).*phi_e1Alpha(:,Nn))),1)*dxn*de;
                end
            end
        end
    end
    fprintf('Rk=%d Rl=%d\n',Rk,Rl)
    if(mod(RflatInd,50)==0)
        save(strcat(savePath,'MTe1',savePathEnd,'.txt'),'MTe1','-ascii','-double');
        save(strcat(savePath,'MVen',savePathEnd,'.txt'),'MVen','-ascii','-double');
        save(strcat(savePath,'dOp1',savePathEnd,'.txt'),'dOp1','-ascii','-double');
        save(strcat(savePath,'dOp2',savePathEnd,'.txt'),'dOp2','-ascii','-double');
        save(strcat(savePath,'dOp3',savePathEnd,'.txt'),'dOp3','-ascii','-double');
    end
end
save(strcat(savePath,'MTe1',savePathEnd,'.txt'),'MTe1','-ascii','-double');
save(strcat(savePath,'MVen',savePathEnd,'.txt'),'MVen','-ascii','-double');
save(strcat(savePath,'dOp1',savePathEnd,'.txt'),'dOp1','-ascii','-double');
save(strcat(savePath,'dOp2',savePathEnd,'.txt'),'dOp2','-ascii','-double');
save(strcat(savePath,'dOp3',savePathEnd,'.txt'),'dOp3','-ascii','-double');

savePathEnd = strcat('RsampleL_',num2str(Rsample(1)),'_RsampleR_',num2str(Rsample(end)),'_dRsample_',num2str(Rsample(2)-Rsample(1)),'_Ne_',num2str(Ne),'_M_',num2str(M),'_mi_',num2str(mi))
