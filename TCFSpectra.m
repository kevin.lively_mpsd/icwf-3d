
function [signal] = TCFSpectra(C,time,energy, ex)

    eta = -log10(1e-3)/time(end);
    mask = exp(-eta*time);
   
    tmp = mask.'.*(C(:,ex));
    signal = zeros(max(size(energy)),1);
    dt = time(2)-time(1);
    for Ei=1:max(size(energy))
        signal(Ei) =  sum(exp(1j*energy(Ei)*time.').*tmp);
    end
    signal = 2*(4*pi/(137))*real(signal.*energy.'*dt);


