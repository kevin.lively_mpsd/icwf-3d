
if(false)

phi_n = load('uniformDriven_dxe_0.3_boxL_20_dx_n_0.05_boxLn_20_softEN_0.1/phiNUniDistRsampleL_0.5_RsampleR_20_dRsample_0.05_Ne_3_M_3_mi_1.txt');

[Delta, Sigma, U] = svd(phi_n(:,2:end),'econ');

minR = min(size(phi_n(:,2:end)));
%Sigma = Sigma(1:minR,1:minR);
tol=1e-6;
keep = [];
for i=1:minR
    if(Sigma(i,i)>tol)
        keep = [keep i];
    end
end
ACan = U(:,keep)*diag(1./sqrt(diag(Sigma(keep,keep))));
chiCan = phi_n(:,2:end)*ACan;
for i=1:max(size(keep))
    chiCan(:,i) = chiCan(:,i)./sqrt(chiCan(:,i)'*chiCan(:,i)*dxn);
end
%save(strcat(savePath,'chiCan.txt'),'chiCan','-ascii','-double');
tmp = [nAxis, chiCan];
save('chiCanTmp.txt','tmp','-ascii','-double');
chiSym = Delta*U';
chi0 = load('chi0.txt');
chi0Spline = spline(chi0(:,1),chi0(:,2),nAxis);
chi0Spline = chi0Spline./sqrt(chi0Spline'*chi0Spline*dxn);

G = chiCan'*chi0Spline*dxn;
C = pinv(chiCan'*chiCan*dxn,1e-12)*G;
tmp = [nAxis, chiCan*C];
save('tmp.txt','tmp','-ascii','-double');

end %DEBUG1

if(true)
%phiCan = load(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/BOCan.mat'));
%phi_e = phiCan.phiCan;

if(Ne>size(phiCan.phiCan,2))
    Ne = size(phiCan.phiCan,2);
    phi_e = phiCan.phiCan;
else
    phi_e = phiCan.phiCan(:,1:Ne);
end

Se1 = phi_e'*phi_e*de;

G = phi_e'*squeeze(BOgs(31,:)')*de;
C = pinv(Se1,1e-12)*G;

end %DEBUG2
