

rxTrajs = repmat(re(:,1),1,NTraj*M);
ryTrajs = repmat(re(:,2),1,NTraj*M);
rzTrajs = repmat(re(:,3),1,NTraj*M);
RTrajs = repmat(nAxis,1,NTraj*M);

Se1_ini = (phi_e1_ini'*phi_e1_ini*de);
Se2_ini = (phi_e2_ini'*phi_e2_ini*de);
Sn_ini = (phi_n_ini'*phi_n_ini*dxn);

dOp1 =  Sn_ini.*Se1_ini.*(phi_e2_ini'*(rxTrajs.*phi_e2_ini)*de) ...
     +  Sn_ini.*Se2_ini.*(phi_e1_ini'*(rxTrajs.*phi_e1_ini)*de);
dOp2 =  Sn_ini.*Se1_ini.*(phi_e2_ini'*(ryTrajs.*phi_e2_ini)*de) ...
     +  Sn_ini.*Se2_ini.*(phi_e1_ini'*(ryTrajs.*phi_e1_ini)*de);
dOp3 =  Sn_ini.*Se1_ini.*(phi_e2_ini'*(rzTrajs.*phi_e2_ini)*de) ...
     +  Sn_ini.*Se2_ini.*(phi_e1_ini'*(rzTrajs.*phi_e1_ini)*de);

ROp = (Se1_ini.*Se2_ini.*(phi_n'*(RTrajs.*phi_n)*dxn));
S_ini = (phi_e1_ini'*phi_e1_ini*de).*(phi_e2_ini'*phi_e2_ini*de).*(phi_n_ini'*phi_n_ini*dxn);
Sinv = pinv(S_ini,tolerance);
for kickDir =1:3

    if(kickDir==1)
        phi_e1 = phi_e1_ini.*exp(1i*1e-4*rxTrajs);
        phi_e2 = phi_e2_ini.*exp(1i*1e-4*rxTrajs);
    elseif(kickDir==2)
        phi_e1 = phi_e1_ini.*exp(1i*1e-4*ryTrajs);
        phi_e2 = phi_e2_ini.*exp(1i*1e-4*ryTrajs);
    elseif(kickDir==3)
        phi_e1 = phi_e1_ini.*exp(1i*1e-4*rzTrajs);
        phi_e2 = phi_e2_ini.*exp(1i*1e-4*rzTrajs);
    end
    init=false;
    setMatrixElements
    Sinv = pinv(S_ini,tolerance);
    propC = -1i*Sinv*H; %propC2 = -1i*Sinv*dOp;
    saveInd=1;

    COrig = C_ini; 
    t=1;
   
    fileNameD = strcat('dipole',num2str(kickDir)); 
    fileNameN = strcat('redN',num2str(kickDir)); 
    fileNameR = strcat('RPos',num2str(kickDir)); 
    fileNameE = strcat('Energy',num2str(kickDir)); 
    dipole = zeros(max(size(saveTime)),4); dipole(:,1) = saveTime;
    RPos = zeros(max(size(saveTime)),1);
    %redN = zeros(nDim,max(size(saveTime))+1); redN(:,1) = nAxis;
    Energy = zeros(max(size(saveTime)),2); Energy(:,1) = saveTime;
    for t=1:max(size(time))
        if(mod(time(t),saveTime(saveInd))==0)
            %saveRDE
            dipole(saveInd,2) = gather(real(C'*dOp1*C));
            dipole(saveInd,3) = gather(real(C'*dOp2*C));
            dipole(saveInd,4) = gather(real(C'*dOp3*C));
            Energy(saveInd,2) = gather(real(C'*H*C));
            RPos(saveInd) = gather(real(C'*ROp*C));
            %redE = zeros(eDimx,2);
            %for i = 1:NTraj*M
            %    redN(:,saveInd+1) = redN(:,saveInd+1) + C(i)*((conj(phi_n).*repmat(phi_n(:,i),1,NTraj*M)))*(conj(C).*Se1(:,i).*Se2(:,i));
                %red_e = red_e + C(i)*((conj(phi_e1).*repmat(phi_e1(:,i),1,NTraj*M)))*(conj(C).*Se2(:,i).*Sn(:,i));
            %end
         
            if(mod(saveTime(saveInd),100)==0)
                save(strcat(savePath,fileNameD,savePathEnd,'.txt'), 'dipole', '-ascii','-double')
                %save(strcat(savePath,fileNameN,savePathEnd,'.txt'), 'redN', '-ascii','-double')
                save(strcat(savePath,fileNameR,savePathEnd,'.txt'), 'RPos', '-ascii','-double')
                save(strcat(savePath,fileNameE,savePathEnd,'.txt'), 'Energy', '-ascii','-double')
            end
            saveInd = saveInd + 1;
        end
        runge_kutta_prop_Cs 
        t = t+1;
    end
    save(strcat(savePath,fileNameD,savePathEnd,'.txt'), 'dipole', '-ascii','-double')
    %save(strcat(savePath,fileNameN,savePathEnd,'.txt'), 'redN', '-ascii','-double')
    save(strcat(savePath,fileNameR,savePathEnd,'.txt'), 'RPos', '-ascii','-double')
    save(strcat(savePath,fileNameE,savePathEnd,'.txt'), 'Energy', '-ascii','-double')
end

