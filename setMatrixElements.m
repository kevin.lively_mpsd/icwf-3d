
fprintf('Setting Matrix Elements\n')
%%Construct Hamiltonian matrix elements

Se1 = phi_e1'*phi_e1*de;
Sn = phi_n'*phi_n*dxn;

S = Se1.*Sn;% + 2*real(Se1e2);
Sinv = pinv(S,tolerance);
%One Body Operators

%Kinetic
MTe1 = Sn.*(phi_e1'*Te*phi_e1*de);
MTn = Se1.*(phi_n'*Tn*phi_n*dxn);

MVnn = Se1.*(phi_n'*(repmat(Vnn,1,NTraj*M).*phi_n)*dxn);

MVen = zeros(NTraj*M);


%Two Body Operators
%Construct upper triangle
for k = 1:NTraj*M
    MVen(k,:) = MVen(k,:) + sum((repmat(conj(phi_n(:,k)),1,NTraj*M).*phi_n).*(Ven*(repmat(conj(phi_e1(:,k)),1,NTraj*M).*phi_e1)),1)*dxn*de;
end

H =  MTn + MTe1 + MVen + MVnn;
U = MVen + MVnn;

if(init==true)
    save(strcat(savePath,'HHartree',savePathEnd,'.txt'),'H','-ascii','-double');
    save(strcat(savePath,'UHartree',savePathEnd,'.txt'),'U','-ascii','-double');
    save(strcat(savePath,'SHartree',savePathEnd,'.txt'),'S','-ascii','-double');
else
    aux = real(H);
    save(strcat(savePath,'HHartreeKick',num2str(kickDir),'Re',savePathEnd,'.txt'),'aux','-ascii','-double');
    aux = imag(H);
    save(strcat(savePath,'HHartreeKick',num2str(kickDir),'Im',savePathEnd,'.txt'),'aux','-ascii','-double');
end
