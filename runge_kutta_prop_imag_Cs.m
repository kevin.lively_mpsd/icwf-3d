
%% K1  
C_dot_1 = propC*C;


%% k2
C_aux = C + C_dot_1*dt_imag/2;
C_aux = C_aux./sqrt(C_aux'*M*C_aux);
C_dot_2 = propC*C_aux;



%% k3
C_aux = C + C_dot_2*dt_imag/2;
C_aux = C_aux./sqrt(C_aux'*M*C_aux);
C_dot_3 = propC*C_aux;


%% k4
C_aux = C + C_dot_3*dt_imag;
C_aux = C_aux./sqrt(C_aux'*M*C_aux);
C_dot_4 = propC*C_aux;

%% EVOLVED CONDITIONAL WAVEFUNCTION AND TRAJECTORIES
C = C + (dt_imag/6)*(C_dot_1 + 2*C_dot_2 + 2*C_dot_3 + C_dot_4);
C = C./sqrt(C'*M*C);
