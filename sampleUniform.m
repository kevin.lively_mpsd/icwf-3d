
fprintf('Uniformly Placing Trajectories\n')




re1 = zeros(2*Ne*max(size(Rsample)),3);
mesh_e1M = zeros(2*Ne*max(size(Rsample)),1);

SBO00 = BOgs*(BOgs')*de;
SBO01 = BOgs*(BOex')*de;
SBO11 = BOex*(BOex')*de;
Se1 = ones(2*Ne*max(size(Rsample))*M);
gsInd = (1:Ne*M);
exInd = (Ne*M+1):2*Ne*M;
for Rk=1:max(size(Rsample))
    [dum, Ri] = min(abs(Rsample(Rk)-nAxis));
    for Rl = Rk:max(size(Rsample))
        [dum, Rj] = min(abs(Rsample(Rl)-nAxis));
        
        %Upper diag block, always
        indL = gsInd + 2*Ne*M*(Rk-1);
        indR = exInd + 2*Ne*M*(Rl-1);
        Se1(indL,indR) = Se1(indL,indR)*SBO01(Ri,Rj);

        if(Ri~=Rj)
            indL = gsInd + 2*Ne*M*(Rk-1);
            indR = gsInd + 2*Ne*M*(Rl-1);
            Se1(indL,indR) = Se1(indL,indR)*SBO00(Ri,Rj);
            
            indL = exInd + 2*Ne*M*(Rk-1);
            indR = gsInd + 2*Ne*M*(Rl-1);
            Se1(indL,indR) = Se1(indL,indR)*SBO01(Ri,Rj);
     
            indL = exInd + 2*Ne*M*(Rk-1);
            indR = exInd + 2*Ne*M*(Rl-1);
            Se1(indL,indR) = Se1(indL,indR)*SBO11(Ri,Rj);
        else
            for Nm=1:2*Ne*M
                for Nn=Nm:2*Ne*M
                    indL = Nm + 2*Ne*M*(Rk-1);
                    indR = Nn + 2*Ne*M*(Rl-1);
                    if(Nm<=Ne*M) % Upper Left
                        Se1(indL,indR) = Se1(indL,indR)*SBO00(Ri,Rj);
                    elseif(Nm>Ne*M) %Lower Right
                        Se1(indL,indR) = Se1(indL,indR)*SBO11(Ri,Rj);
                    end
                end
            end
        end 
    end
end

Se1 = Se1 - tril(Se1) + triu(Se1)';

for Rk=1:max(size(Rsample))
    [dum, Ri] = min(abs(Rsample(Rk)-nAxis));
    cgs = cumsum(abs(BOgs(Ri,:)).^2*de);
    cex = cumsum(abs(BOex(Ri,:)).^2*de);

    for Ni=1:2*Ne
        ind = Ni+2*Ne*(Ri-1);
        if(Ni <=Ne)
            [dum, mesh_e1M(ind)] = min(abs(rand(1)-cgs));
        else
            [dum, mesh_e1M(ind)] = min(abs(rand(1)-cex));
        end
        re1(ind,1) = re(mesh_e1M(ind),1) + dxe*(0.5-rand(1));
        re1(ind,2) = re(mesh_e1M(ind),2) + dxe*(0.5-rand(1));
        re1(ind,3) = re(mesh_e1M(ind),3) + dxe*(0.5-rand(1));
    end
end

valsDiag = [-2*ones(nDim,1)];
vals = [ones(nDim,1)];
nLapl = spdiags(valsDiag,0,nDim,nDim) ...
      + spdiags(vals, 1, nDim,nDim)     + spdiags(vals, -1, nDim,nDim);

nLapl = (1/dxn^2)*nLapl; 
Tn = (-1/(2*mu_n))*nLapl;

phi_n = zeros(nDim,2*Ne*max(size(Rsample)));

for Ni=1:2*Ne*max(size(Rsample))
       Vn_ea = Z(1)./sqrt( (re1(Ni,1) - (m1/(m1+m2))*nAxis).^2 ...
              +          (re1(Ni,2)).^2 ...
              +          (re1(Ni,3)).^2  + softEN) ...
              +      Z(1)./sqrt( (re1(Ni,1) + (m2/(m1+m2))*nAxis).^2 ...
              +          (re1(Ni,2)).^2 ...
              +          (re1(Ni,3)).^2 + softEN);
    [eigVN,eigN] = eigs(Tn + spdiags(Vn_ea + 1./nAxis,0,nDim,nDim),M,'sa');
    for Mi=1:M
        phi_n(:, Mi + M*(Ni-1)) = eigVN(:,Mi)./sqrt(eigVN(:,Mi)'*eigVN(:,Mi)*dxn);
        phiEnergyN(Mi+M*(Ni-1)) = eigN(Mi,Mi);
    end
    fprintf('%d percent done\n', 100*Ni/(2*Ne*max(size(Rsample))))
end

tmp = [nAxis, phi_n];
save(strcat(savePath,'phiNUniDist',savePathEnd,'.txt'),'tmp','-ascii','-double');
tmp = [nAxis, mean(abs(phi_n),2)];
save(strcat(savePath,'phiNUniAbsAvg',savePathEnd,'.txt'),'tmp','-ascii','-double');
save(strcat(savePath,'phiNUniEDist',savePathEnd,'.txt'),'phiEnergyN','-ascii','-double');
