function [x0,y0,z0]=mcsampling(PDaux,N_traj_aux,threshold)

%define the possible values for the (x,y) pair
PDaux = PDaux/sum(PDaux(:));

x_vals = kron([1:size(PDaux,1)]',kron(ones(1,size(PDaux,2)),ones(1,size(PDaux,3))));  %all x values
y_vals = kron(ones(size(PDaux,1),1),kron([1:size(PDaux,2)],ones(1,size(PDaux,3))));  %all y values
z_vals = kron(ones(size(PDaux,1),1),kron(ones(1,size(PDaux,2)),[1:size(PDaux,3)]));  %all z values

%convert your 2D problem into a 1D problem
PDaux = PDaux(:);
PDaux(PDaux<threshold*max(PDaux(:))) = 1E-12*max(PDaux(:));
x_vals = x_vals(:);
y_vals = y_vals(:);
z_vals = z_vals(:);

%calculate your fake 1D CDF, assumes sum(A(:))==1
CDF = cumsum(PDaux);%cumsum(PDaux); %remember, first term out of of cumsum is not zero

%because of the operation we're doing below (interp1 followed by ceil)
%we need the CDF to start at zero
CDF = [0; CDF(:)];

%generate random values
rand_vals = rand(N_traj_aux,1);  %spans zero to one

%look into CDF to see which index the rand val corresponds to
out_val = interp1(CDF,[0:1/(length(CDF)-1):1],rand_vals); %spans zero to one
ind = ceil(out_val*length(PDaux));

%using the inds, you can lookup each pair of values
ind(isnan(ind)) = [];

x0 = x_vals(ind);
y0 = y_vals(ind);
z0 = z_vals(ind);
