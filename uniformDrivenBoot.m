
if(exist('mi')==0)
    mi=1;
end
if(exist('saveCWFs')==0)
    saveCWFs = false;
end

%Number of electrons and nuclei
Nele = 1;
Nnuc = 1; %H2 molecule, single vibrational degree of freedom

%Simulation box length
boxL = 20; uniformBox=true;
a = 2*boxL; dxe = 0.3;
boxLn = 20;

%nuclear and electronic grid spacing
dxn = 0.05;

%electron grid volume element
de = 1;
for ei=1:3
    de = de*dxe;
end

%Charge of ions as seen from electrons
Z = [-1];
m1 = 1836; m2 =1836; mu_n = m1*m2/(m1+m2); 

mu_e = 2*m1/(2*m1 + 1);

%CWF softening parameters,
softEN = 0.1;



%x, y and z axes and dimensions. 
if(uniformBox == true)
    eBoxLx = -boxL; eBoxRx = boxL; eAxisx = (eBoxLx:dxe:eBoxRx).'; eDimx = max(size(eAxisx));
    eBoxLy = -boxL; eBoxRy = boxL; eAxisy = (eBoxLy:dxe:eBoxRy).'; eDimy = max(size(eAxisy));
    eBoxLz = -boxL; eBoxRz = boxL; eAxisz = (eBoxLz:dxe:eBoxRz).'; eDimz = max(size(eAxisz));

    eAxis = [eAxisx, eAxisy, eAxisz];

    nBoxL = 0.5; nBoxR = boxLn; nAxis = (nBoxL:dxn:nBoxR).'; nDim = max(size(nAxis));
end

%Total number of grid points
eDim = eDimx*eDimy*eDimz;

%Box parameters, may be useful later
eBoxL = [eBoxLx, eBoxLy, eBoxLz]; 
eBoxR = [eBoxRx, eBoxRy, eBoxRz]; 
eBoxDims = [eDimx, eDimy, eDimz];

nBoxL = [nBoxL]; 
nBoxR = [nBoxR]; 
nBoxDims = [nDim];

%Time steps
dt = 0.001; dt_imag=0.01;
Tf = 200; time = 0:dt:Tf;
saveInt = floor(0.1/dt);
saveTime = time(1:saveInt:end);

saveIntNuc = floor(10/dt);
saveTimeNuc = time(1:saveIntNuc:end);

%oml = load(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_nBoxL_',num2str(nBoxL),'_softEN_',num2str(softEN),'/oml.txt'));
oml = load(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/oml.txt'));

tau = nPeriods*(2*pi/oml); 

A0 = sqrt(2*I/3.50944758e16)/oml;
Agauge = A0*cos((pi/(tau))*(time-tau/2)).^2 .* cos(oml*(time-tau/2) + pi/2);
Agauge(time>tau)=0;
Efield = gradient(Agauge,dt);

%savePath = strcat('uniformDriven_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_nBoxL_',num2str(nBoxL),'_softEN_',num2str(softEN),'/');
savePath = strcat('uniformDriven_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/');
savePathEnd = strcat('RsampleL_',num2str(Rsample(1)),'_RsampleR_',num2str(Rsample(end)),'_dRsample_',num2str(Rsample(2)-Rsample(1)),'_Ne_',num2str(Ne),'_M_',num2str(M),'_mi_',num2str(mi))
mkdir(savePath);

tmp=[time.',Agauge.'];
save(strcat(savePath,'Agauge.txt'),'tmp','-ascii','-double');
tmp=[time.',Efield.'];
save(strcat(savePath,'Efield.txt'),'tmp','-ascii','-double');

init=true;

%pinv tol
tolerance=1e-4;
