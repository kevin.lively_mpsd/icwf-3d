mi=1;rng(mi);
%Ne = 1;
%M=3;
nPeriods = 5;
I = 5e13; %W/cm^2

newCWF=false;
if(newCWF==false)
    saveCWFs=true;
end

lowdinDrivenBoot

loadUniformOp
%loadLowdinMatrixElements
setLowdinMatrixElements


a = dir(strcat(savePath,'CHartree',savePathEnd,'.txt'));
if(min(size(a))==0) %If this is not found
    Imaginary_time_propCs
else
    a = dir(strcat(savePath,'CRe',savePathEnd,'.txt')); %look for the proper on
    if(min(size(a))==0) % If this is not found 
        Imaginary_time_propCs
    else
        fprintf('Loading ground state expansion tensor\n');
        C_ini = complex(load(strcat(savePath,'CRe',savePathEnd,'.txt')), ...
                        load(strcat(savePath,'CIm',savePathEnd,'.txt')));
    end
end

H_CsLowdinDriven
