
fprintf('Propagating Uniform Sampling\n')

for pumpDir = 0:2
    COrig=C_ini;
    C = COrig; 
    
    propC = -1i*Sinv*H;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Change After Debug %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if(pumpDir==0)
        Efield=0*Efield;
        propC2 = -1i*Sinv*dOp1;
    elseif(pumpDir==1)
        %propC2 = -1i*Sinv*dOp2;
        Efield = gradient(Agauge,dt);
        propC2 = -1i*Sinv*dOp1;
    elseif(pumpDir==2)
        propC2 = -1i*Sinv*dOp2;
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    saveInd=1; 
    saveIndNuc=1; 
    fileNameEnd=strcat('_dt_',num2str(dt),'_I_',num2str(log10(I)),'_tau_',num2str(tau),'_oml_',num2str(oml),'_dir_',num2str(pumpDir),'_');

    fileNameD = strcat('dipole',fileNameEnd); 
    fileNameN = strcat('redN',  fileNameEnd); 
    fileNameR = strcat('RPos',  fileNameEnd); 
    %fileNameK = strcat('KERKernel',  fileNameEnd); 
    fileNameE = strcat('Energy',fileNameEnd); 
    dipole = zeros(max(size(saveTime)),4); dipole(:,1) = saveTime;
    RPos = zeros(max(size(saveTime)),1);
    redN = zeros(nDim,max(size(saveTimeNuc))+1); redN(:,1) = nAxis;
    KERKern = zeros(nDim,max(size(saveTimeNuc)));
    Energy = zeros(max(size(saveTime)),2); Energy(:,1) = saveTime;
    for t=1:max(size(time))
        if(mod(time(t),saveTime(saveInd))==0)
            %saveRDE
            dipole(saveInd,2) = gather(real(C'*dOp1*C));
            dipole(saveInd,3) = gather(real(C'*dOp2*C));
            dipole(saveInd,4) = gather(real(C'*dOp3*C));
            Energy(saveInd,2) = gather(real(C'*H*C));
            RPos(saveInd) = gather(real(C'*ROp*C));

            if(mod(time(t),saveTimeNuc(saveIndNuc))==0)
                %redE = zeros(eDimx,2);
                
                redN(:,saveIndNuc+1) = lowdinNucDen(redNOp,Se1,C);
                %KERKern(:,saveIndNuc) = KERKernOp*C;
                redN = real(redN);
                saveIndNuc = saveIndNuc+1;
            end
            if(mod(saveTime(saveInd),1)==0)
                fprintf('%f percent done\n',100*time(t)/time(end))
                save(strcat(savePath,fileNameD,savePathEnd,'.txt'), 'dipole', '-ascii','-double')
                save(strcat(savePath,fileNameN,savePathEnd,'.txt'), 'redN', '-ascii','-double')
                %save(strcat(savePath,fileNameK,savePathEnd,'.txt'), 'KERKern', '-ascii','-double')
                save(strcat(savePath,fileNameR,savePathEnd,'.txt'), 'RPos', '-ascii','-double')
                save(strcat(savePath,fileNameE,savePathEnd,'.txt'), 'Energy', '-ascii','-double')
            end
            saveInd = saveInd + 1;
        end
        if(time(t)<= tau)
            %runge_kutta_prop_FieldCsMinRes
            runge_kutta_prop_FieldCs
        else
            %runge_kutta_prop_CsMinRes
            runge_kutta_prop_Cs
        end
    end

    save(strcat(savePath,fileNameD,savePathEnd,'.txt'), 'dipole', '-ascii','-double')
    save(strcat(savePath,fileNameN,savePathEnd,'.txt'), 'redN', '-ascii','-double')
    %save(strcat(savePath,fileNameK,savePathEnd,'.txt'), 'KERKern', '-ascii','-double')
    save(strcat(savePath,fileNameR,savePathEnd,'.txt'), 'RPos', '-ascii','-double')
    save(strcat(savePath,fileNameE,savePathEnd,'.txt'), 'Energy', '-ascii','-double')
end


