
if(exist('mi')==0)
    mi=1;
end
if(exist('saveCWFs')==0)
    saveCWFs = false;
end
%Number of electrons and nuclei
Nele = 1;
Nnuc = 1; %He atom, pinned classical proton

%Simulation box length
boxL = 15; uniformBox=true;
a = 2*boxL; dxe = 0.3;
boxLn = 20;

%nuclear and electronic grid spacing
dxn = 0.05;

%electron grid volume element
de = 1;
for ei=1:3
    de = de*dxe;
end


%Charge of ions
Z = [-1];
m1 = 1836; m2 =1836; mu_n = m1*m2/(m1+m2); 
mu_e = 1;


%CWF softening parameters,
softEN = 0.1;


%x, y and z axes and dimensions. 
if(uniformBox == true)
    eBoxLx = -boxL; eBoxRx = boxL; eAxisx = (eBoxLx:dxe:eBoxRx).'; eDimx = max(size(eAxisx));
    eBoxLy = -boxL; eBoxRy = boxL; eAxisy = (eBoxLy:dxe:eBoxRy).'; eDimy = max(size(eAxisy));
    eBoxLz = -boxL; eBoxRz = boxL; eAxisz = (eBoxLz:dxe:eBoxRz).'; eDimz = max(size(eAxisz));

    eAxis = [eAxisx, eAxisy, eAxisz];

    
    nBoxL = 0.5; nBoxR = boxLn; nAxis = (nBoxL:dxn:nBoxR).'; nDim = max(size(nAxis));
end

%Total number of grid points
eDim = eDimx*eDimy*eDimz;

%Box parameters, may be useful later
eBoxL = [eBoxLx, eBoxLy, eBoxLz]; 
eBoxR = [eBoxRx, eBoxRy, eBoxRz]; 
eBoxDims = [eDimx, eDimy, eDimz];

nBoxL = [nBoxL]; 
nBoxR = [nBoxR]; 
nBoxDims = [nDim];

%Time steps
dt = 0.001; dt_imag=0.01;
Tf = 1000; time = 0:dt:Tf;
saveInt = floor(0.1/dt);
saveTime = time(1:saveInt:end);

savePath = strcat('dump_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/');
savePathEnd = strcat('_NTraj_',num2str(NTraj),'_M_',num2str(M),'_mi_',num2str(mi))
mkdir(savePath);
init=true;

%pinv tol
tolerance=1e-8;
