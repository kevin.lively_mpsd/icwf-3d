%% IMAGINARY-TIME INITIALIZATION OF CWFs AND Cs:

fprintf('Running Imaginary Time Evolution\n');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INITIAL Cs FROM INITIAL CWFs BY PSEUDO-INVERSE %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%ExactInterR = 2.75;
%gausX = exp(-(eAxisx-ExactInterR/2).^2) + exp(-(eAxisx + ExactInterR/2).^2); gausX = gausX./sqrt(gausX'*gausX*dxe);
%gausY = exp(-(eAxisx).^2); gausY = gausY./sqrt(gausY'*gausY*dxe);
%red_n_gr = exp(-(nAxis - ExactInterR).^2/0.25^2); red_n_gr = red_n_gr./sqrt(red_n_gr'*red_n_gr*dxn);
%red_e_gr = kron(gausX,kron(gausY,gausY)); 
%red_e_gr = red_e_gr./sqrt(sum(red_e_gr)*de);

%G = zeros(NTraj*M,1);
G = rand(size(H,1),1); G(max(floor(max(size(G))/2),1):end)=0;
%for alpha = 1:NTraj*M
%    G(alpha) = (phi_n(:,alpha)'*red_n_gr*dxn).*(phi_e1(:,alpha)'*red_e_gr*de);    
%end
Sinv = pinv(S,tolerance);
C = Sinv*G;
C = C/sqrt(C'*(S*C));
M = S;
a = dir(strcat(savePath,'CHartree',savePathEnd,'.txt')); %look for a starting position
if(min(size(a))>0) %Then we can restart
    C = load(strcat(savePath,'CHartree',savePathEnd,'.txt'));
end
propC = -1*Sinv*H;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% IMAGINARY-TIME PROPAGATION %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t = 0;
time_index = 0;
time_start = clock;
time_step = time_start;
time_int=10;
if(GPU==true)
    M = gpuArray(M);
    C = gpuArray(C);
    propC = gpuArray(propC);
    HGPU = gpuArray(H);
    E_tot = real(C'*(HGPU)*C);
else
    E_tot = real(C'*(H)*C);
end


E_tot_old = E_tot + 1;
while abs(E_tot - E_tot_old)/abs(E_tot) > 1E-12
    t = t + 1;
    
    if (mod(t,time_int) == 0 || t == 1) 
        
        E_tot_old = E_tot;
        %disp(E_tot)
        %disp(energy_gr)
    end
    
    runge_kutta_prop_imag_Cs

    E_tot = real(C'*(H)*C);
    %max(abs(imag(C)))
    
end
C_ini = C;
Ctmp = gather(C);
tmp = real(Ctmp);
save(strcat(savePath,'CRe',savePathEnd,'.txt'),'tmp','-ascii','-double');
tmp = imag(Ctmp);
save(strcat(savePath,'CIm',savePathEnd,'.txt'),'tmp','-ascii','-double');

