

rxTrajs = repmat(re(:,1),1,NTraj*M);
ryTrajs = repmat(re(:,2),1,NTraj*M);
rzTrajs = repmat(re(:,3),1,NTraj*M);
RTrajs = repmat(nAxis,1,NTraj*M);

Se1_ini = (phi_e1_ini'*phi_e1_ini*de);
Sn_ini = (phi_n_ini'*phi_n_ini*dxn);

Se1 = Se1_ini;
Sn = Sn_ini;

dOp1 =  -1*Sn.*(phi_e1'*(rxTrajs.*phi_e1)*de);
dOp2 =  -1*Sn.*(phi_e1'*(ryTrajs.*phi_e1)*de);
dOp3 =  -1*Sn.*(phi_e1'*(rzTrajs.*phi_e1)*de);

kickOp1 = 1e-3*dOp1;
kickOp2 = 1e-3*dOp2;
kickOp3 = 1e-3*dOp3;

ROp = (Se1.*(phi_n'*(RTrajs.*phi_n)*dxn));

for kickDir =0:3

    if(kickDir==1)
        phi_e1Kick = kickOp1.*phi_e1;
        phi_e2Kick = kickOp1.*phi_e2;
    elseif(kickDir==2)
        phi_e1Kick = kickOp2.*phi_e1;
        phi_e2Kick = kickOp2.*phi_e2;
    elseif(kickDir==3)
        phi_e1Kick = kickOp3.*phi_e1;
        phi_e2Kick = kickOp3.*phi_e2;
    elseif(kickDir==0)
        phi_e1Kick = phi_e1;
        phi_e2Kick = phi_e2;
    end
    MTe1 = Sn.*(phi_e1Kick'*(Te*phi_e1Kick*de));
    MTn =  Se1.*(phi_n'*(Tn*phi_n*dxn));

    H = U + MTe1 + MTn;

    COrig=C_ini;
    propC = -1i*Sinv*H; %propC2 = -1i*Sinv*dOp;

    saveInd=1;

    C = COrig; 
    t=1;
   
    fileNameD = strcat('dipole',num2str(kickDir)); 
    %fileNameN = strcat('redN',num2str(kickDir)); 
    fileNameR = strcat('RPos',num2str(kickDir)); 
    fileNameE = strcat('Energy',num2str(kickDir)); 
    dipole = zeros(max(size(saveTime)),4); dipole(:,1) = saveTime;
    RPos = zeros(max(size(saveTime)),1);
    %redN = zeros(nDim,max(size(saveTime))+1); redN(:,1) = nAxis;
    Energy = zeros(max(size(saveTime)),2); Energy(:,1) = saveTime;
    for t=1:max(size(time))
        if(mod(time(t),saveTime(saveInd))==0)
            %saveRDE
            dipole(saveInd,2) = gather(real(C'*dOp1*C));
            dipole(saveInd,3) = gather(real(C'*dOp2*C));
            dipole(saveInd,4) = gather(real(C'*dOp3*C));
            Energy(saveInd,2) = gather(real(C'*H*C));
            RPos(saveInd) = gather(real(C'*ROp*C));
            %redE = zeros(eDimx,2);
            %for i = 1:NTraj*M
            %    redN(:,saveInd+1) = redN(:,2) + C(i)*((conj(phi_n).*repmat(phi_n(:,i),1,NTraj*M)))*(conj(C).*Se1(:,i).*Se2(:,i));
               %red_e = red_e + C(i)*((conj(phi_e1).*repmat(phi_e1(:,i),1,NTraj*M)))*(conj(C).*Se2(:,i).*Sn(:,i));
            %end
         
            if(mod(saveTime(saveInd),100)==0)
                save(strcat(savePath,fileNameD,savePathEnd,'.txt'), 'dipole', '-ascii','-double')
                %save(strcat(savePath,fileNameN,savePathEnd,'.txt'), 'redN', '-ascii','-double')
                save(strcat(savePath,fileNameR,savePathEnd,'.txt'), 'RPos', '-ascii','-double')
                save(strcat(savePath,fileNameE,savePathEnd,'.txt'), 'Energy', '-ascii','-double')
            end
            saveInd = saveInd + 1;
        end
        runge_kutta_prop_Cs 
    end
    energy=0:0.001:1;
    a1 = KickSpectra(dipole,saveTime,1e-3,energy, kickDir);
    a1 = [energy.' a1];


    save(strcat(savePath,fileNameD,savePathEnd,'.txt'), 'dipole', '-ascii','-double')
    %save(strcat(savePath,fileNameN,savePathEnd,'.txt'), 'redN', '-ascii','-double')
    save(strcat(savePath,fileNameR,savePathEnd,'.txt'), 'RPos', '-ascii','-double')
    save(strcat(savePath,fileNameE,savePathEnd,'.txt'), 'Energy', '-ascii','-double')
    save(strcat(savePath,'Abs',num2str(kickDir),savePathEnd,'.txt'), 'a1', '-ascii','-double')
end


