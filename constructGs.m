if(true)
    BOs = load('BO_dxe_0.3_boxL_20_dx_n_0.1_boxLn_15_softEN_0.1/BOstates.mat');
    BOs = BOs.BOstates;
end
BOenergy = load('BO_dxe_0.3_boxL_20_dx_n_0.1_boxLn_15_softEN_0.1/BOenergy.txt');

boxL = 20; uniformBox=true;
a = 2*boxL; dxe = 0.3;
boxLn = 15;

%nuclear and electronic grid spacing
dxn = 0.1;

de = 1;
for ei=1:3
    de = de*dxe;
end

ex= 2;

%Charge of ions
Z = [-1];
m1 = 1836; m2 =1836; mu_n = m1*m2/(m1+m2); 

mu_e = 2*m1/(2*m1 + 1);

%CWF softening parameters,
softEN = 0.1;
if(uniformBox == true)
    eBoxLx = -boxL; eBoxRx = boxL; eAxisx = (eBoxLx:dxe:eBoxRx).'; eDimx = max(size(eAxisx));
    eBoxLy = -boxL; eBoxRy = boxL; eAxisy = (eBoxLy:dxe:eBoxRy).'; eDimy = max(size(eAxisy));
    eBoxLz = -boxL; eBoxRz = boxL; eAxisz = (eBoxLz:dxe:eBoxRz).'; eDimz = max(size(eAxisz));

    eAxis = [eAxisx, eAxisy, eAxisz];

    nBoxL = 0.5; nBoxR = boxLn; nAxis = (nBoxL:dxn:nBoxR).'; nDim = max(size(nAxis));
end

%Total number of grid points
eDim = eDimx*eDimy*eDimz;

%Box parameters, may be useful later
eBoxL = [eBoxLx, eBoxLy, eBoxLz]; 
eBoxR = [eBoxRx, eBoxRy, eBoxRz]; 
eBoxDims = [eDimx, eDimy, eDimz];

nBoxL = [nBoxL]; 
nBoxR = [nBoxR]; 
nBoxDims = [nDim];

savePath = strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/');

valsDiag = [-2*ones(nDim,1)];
vals = [ones(nDim,1)];
nLapl = spdiags(valsDiag,0,nDim,nDim) ...
      + spdiags(vals, 1, nDim,nDim)     + spdiags(vals, -1, nDim,nDim);

nLapl = (1/dxn^2)*nLapl; 
Tn = (-1/(2*mu_n))*nLapl;

re = zeros(eDim,3);
boundaryIndex = [];
for zi=1:eDimz
    for yi=1:eDimy
        m = (1:eDimz) + eDimy*(yi-1 + eDimz*(zi-1)); %flattened index
        re(m,1) = eAxisx(:);
        re(m,2) = eAxisy(yi);
        re(m,3) = eAxisz(zi);
        if(yi==1 || yi==eDimy || zi==1 || zi==eDimz)
            boundaryIndex = [boundaryIndex m];
        else
            boundaryIndex = [boundaryIndex m(1) m(end)];
        end
        %g(m,1) = gAxis(:); 
        %g(m,2) = gAxis(yi); 
        %g(m,3) = gAxis(zi); 
    end
end

[chi0, chiE] = eigs(Tn + diag(BOenergy(:,2)),1,'sa');

chi0 = chi0./sqrt(chi0'*chi0*dxn);
save(strcat(savePath,'chi0.txt'),'chi0','-ascii','-double');

[dum, minRi] = min(BOenergy(:,2));

oml = BOenergy(minRi,3)-chiE;
save(strcat(savePath,'oml.txt'),'oml','-ascii','-double');

redEx = zeros(eDimx,1);
redEy = zeros(eDimy,1);
redEz = zeros(eDimz,1);

BO0 = reshape(BOs(:,:,1),nDim,eDim);
BO0Flat = reshape(BO0,nDim,eDimx,eDimy,eDimz);
for Ri=1:nDim
    redEx = redEx + abs(chi0(Ri)).^2*dxn*reshape(sum(sum(abs(BO0Flat(Ri,:,:,:)).^2,4),3)*dxe^2,eDimx,1);
    redEy = redEy + abs(chi0(Ri)).^2*dxn*reshape(sum(sum(abs(BO0Flat(Ri,:,:,:)).^2,4),2)*dxe^2,eDimy,1);
    redEz = redEz + abs(chi0(Ri)).^2*dxn*reshape(sum(sum(abs(BO0Flat(Ri,:,:,:)).^2,2),3)*dxe^2,eDimz,1);
end

save(strcat(savePath,'redEx.txt'),'redEx','-ascii','-double');
save(strcat(savePath,'redEy.txt'),'redEy','-ascii','-double');
save(strcat(savePath,'redEz.txt'),'redEz','-ascii','-double');



