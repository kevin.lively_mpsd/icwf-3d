fprintf('Loading Matrix Elements\n')

H = load(strcat(savePath,'HHartree',savePathEnd,'.txt'));
S = load(strcat(savePath,'SHartree',savePathEnd,'.txt'));
Sinv = pinv(S,tolerance);
U = load(strcat(savePath,'SHartree',savePathEnd,'.txt'));
C = load(strcat(savePath,'CHartree',savePathEnd,'.txt'));
C_ini = C;
