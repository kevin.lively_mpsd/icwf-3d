function basisOut = CanonicalLowdinOrthogonalize(basisIn, tol)
    %Takes a basis of shape dimension x N_bases 
    % Returns one based on the input tolerance

    [Delta,Sigma,U] = svd(basisIn,'econ');
    
    keep = [];
    for i=1:min(size(Sigma))
        if(Sigma(i,i)>tol)
            keep = [keep i];
        end
    end
    ACan = U(:,keep)*diag(1./sqrt(diag(Sigma(keep,keep))));
    
    basisOut = basisInACan;
    for i=1:max(size(keep))
        basisOut(:,i) = basisOut(:,i)./sqrt(basisOut(:,i)'*basisOut(:,i)*de);
    end
