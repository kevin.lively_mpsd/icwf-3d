fprintf(strcat('Propagating I=', num2str(log10(I)), ' tau=',num2str(tau),'\n'))
COrig=C_ini;
C = COrig; 

if(GPU==true) 
    propC = -1i*Sinv*HGPU;
else
    propC = -1i*Sinv*H;
end
%propC2 = -1i*Sinv*dOp2;
Efield = gradient(Agauge,dt);
if(GPU==true)
    propC2 = -1i*Sinv*dOp1GPU;
else
    propC2 = -1i*Sinv*dOp1;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

saveInd=1; 
saveIndNuc=1; 

fileNameD = strcat('dipole',fileNameEnd); 
fileNameN = strcat('redN',  fileNameEnd); 
fileNameR = strcat('RPos',  fileNameEnd); 
fileNameGsRe = strcat('GsProjRe',  fileNameEnd); 
fileNameGsIm = strcat('GsProjIm',  fileNameEnd); 
fileNameExRe = strcat('ExProjRe',  fileNameEnd); 
fileNameExIm = strcat('ExProjIm',  fileNameEnd); 
fileNameE = strcat('Energy',fileNameEnd); 
dipole = zeros(max(size(saveTime)),4); dipole(:,1) = saveTime;
RPos = zeros(max(size(saveTime)),1);
redN = zeros(nDim,max(size(saveTimeNuc))+1); redN(:,1) = nAxis;
GsProj = zeros(nDim,max(size(saveTimeNuc))+1); GsProj(:,1) = nAxis;
ExProj = zeros(nDim,max(size(saveTimeNuc))+1); ExProj(:,1) = nAxis;
Energy = zeros(max(size(saveTime)),2); Energy(:,1) = saveTime;
for t=1:max(size(time))
    if(saveIndNuc <= max(size(saveTimeNuc)) && mod(t-1,saveIntNuc)==0)
        Ctmp = gather(C);
        %redE = zeros(eDimx,2);
        
        redN(:,saveIndNuc+1) = lowdinNucDen(redNOp,Se1,Ctmp);
        %KERKern(:,saveIndNuc) = KERKernOp*C;
        tmpGs = zeros(nDim,1);
        tmpEx = zeros(nDim,1);
        for ii=1:Ne
            tmpGs = tmpGs + (repmat(Fg(:,ii),1,Nn).*phi_n)*Ctmp((1:Nn) + Nn*(ii-1));
            tmpEx = tmpEx + (repmat(Fe(:,ii),1,Nn).*phi_n)*Ctmp((1:Nn) + Nn*(ii-1));
        end
        GsProj(:,saveIndNuc+1) = tmpGs;
        ExProj(:,saveIndNuc+1) = tmpEx;
        redN = real(redN);
        saveIndNuc = saveIndNuc+1;
    end
    if(saveInd <= max(size(saveTime)) && mod(t-1,saveInt)==0)
        Ctmp = gather(C);
        %saveRDE
        dipole(saveInd,2) = gather(real(Ctmp'*dOp1*Ctmp));
        dipole(saveInd,3) = gather(real(Ctmp'*dOp2*Ctmp));
        dipole(saveInd,4) = gather(real(Ctmp'*dOp3*Ctmp));
        Energy(saveInd,2) = gather(real(Ctmp'*H*Ctmp));
        RPos(saveInd) = gather(real(Ctmp'*ROp*Ctmp));

        saveInd = saveInd + 1;
    end
    if(mod(t,floor(1/dt))==0)
        fprintf('%f percent done\n',100*time(t)/time(end))
        save(strcat(savePath,fileNameD,savePathEnd,'.txt'), 'dipole', '-ascii','-double')
        save(strcat(savePath,fileNameN,savePathEnd,'.txt'), 'redN', '-ascii','-double')
        %save(strcat(savePath,fileNameK,savePathEnd,'.txt'), 'KERKern', '-ascii','-double')
        tmp = real(GsProj);
        save(strcat(savePath,fileNameGsRe,savePathEnd,'.txt'), 'tmp', '-ascii','-double')
        tmp = imag(GsProj); tmp(:,1) = nAxis;
        save(strcat(savePath,fileNameGsIm,savePathEnd,'.txt'), 'tmp', '-ascii','-double')
        tmp = real(ExProj);
        save(strcat(savePath,fileNameExRe,savePathEnd,'.txt'), 'tmp', '-ascii','-double')
        tmp = imag(ExProj); tmp(:,1) = nAxis;
        save(strcat(savePath,fileNameExIm,savePathEnd,'.txt'), 'tmp', '-ascii','-double')
        save(strcat(savePath,fileNameR,savePathEnd,'.txt'), 'RPos', '-ascii','-double')
        save(strcat(savePath,fileNameE,savePathEnd,'.txt'), 'Energy', '-ascii','-double')
    end
    %tic
    if(time(t)<= tau)
        %runge_kutta_prop_FieldCsMinRes
        runge_kutta_prop_FieldCs
    else
        %runge_kutta_prop_CsMinRes
        runge_kutta_prop_Cs
    end
    %toc
end

save(strcat(savePath,fileNameD,savePathEnd,'.txt'), 'dipole', '-ascii','-double')
save(strcat(savePath,fileNameN,savePathEnd,'.txt'), 'redN', '-ascii','-double')
%save(strcat(savePath,fileNameK,savePathEnd,'.txt'), 'KERKern', '-ascii','-double')
        tmp = real(GsProj);
        save(strcat(savePath,fileNameGsRe,savePathEnd,'.txt'), 'tmp', '-ascii','-double')
        tmp = imag(GsProj); tmp(:,1) = nAxis;
        save(strcat(savePath,fileNameGsIm,savePathEnd,'.txt'), 'tmp', '-ascii','-double')
        tmp = real(ExProj);
        save(strcat(savePath,fileNameExRe,savePathEnd,'.txt'), 'tmp', '-ascii','-double')
        tmp = imag(ExProj); tmp(:,1) = nAxis;
        save(strcat(savePath,fileNameExIm,savePathEnd,'.txt'), 'tmp', '-ascii','-double')
save(strcat(savePath,fileNameR,savePathEnd,'.txt'), 'RPos', '-ascii','-double')
save(strcat(savePath,fileNameE,savePathEnd,'.txt'), 'Energy', '-ascii','-double')
