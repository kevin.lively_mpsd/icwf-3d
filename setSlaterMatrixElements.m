
fprintf('Setting Matrix Elements\n')
%%Construct Hamiltonian matrix elements

%TODO: clean up, incorporate slater determinant algebra consistently 
Se1 = phi_e1'*phi_e1*de;
Se2 = phi_e2'*phi_e2*de;
Se1e2 = phi_e1'*phi_e2*de;
Se2e1 = Se1e2';

%S = 0.5*Se1.*Se2 - 0.5*Se1e2.*Se2e1 - 0.5*Se2e1.*Se1e2 + 0.5*Se2.*Se1;
S = 2*Se1.*Se2 - 2*Se1e2.*Se2e1; %2 * (1/2) implicit 
%One Body Operators

%Kinetic
%MT = 0.5*(phi_e1'*Te*phi_e1*de).*Se2 - 0.5*(phi_e1'*Te*phi_e2*de).*Se2e1 - 0.5*(phi_e2'*Te*phi_e1*de).*Se1e2...
%   + 0.5*(phi_e2'*Te*phi_e2*de).*Se1; %2 * (1/2) implict

MTe = (phi_e1'*Te*phi_e1*de).*Se2 ... 
   - (phi_e1'*Te*phi_e2*de).*Se2e1 ...
   - (phi_e2'*Te*phi_e1*de).*Se1e2 ...
   + (phi_e2'*Te*phi_e2*de).*Se1; %2 times of those operators in the hamiltonian cancel 0.5
MTe = 2*MTe; 

%External Potential 
%TODO Fix this to take the exact external potential, not soft.
VeExtOp = repmat(VeExt,1,NTraj*M);

cross = (phi_e1'*(VeExtOp.*phi_e2*de)).*Se2e1; 
MVeExt = Se1.*(phi_e2'*(VeExtOp.*phi_e2))*de ...
       - (cross +cross') ...
       + Se2.*(phi_e1'*(VeExtOp.*phi_e1))*de; %2 times of these operators in the hamiltonian cancel 0.5

MVeExt = 2*MVeExt;
%Two Body Operators
%TODO: slater determinant algebra
MVee = zeros(NTraj*M);
tmp = conj(phi_e2(:,1)).*phi_e2(:,1);
x1 = -VeExt.*tmp;
x2 = x1;
x3 = x1;
x4 = x1;
%Construct upper triangle
for alpha = 1:NTraj*M
    for beta=alpha:NTraj*M

        %Mixed-index densities
        tmp = conj(phi_e2(:,alpha)).*phi_e2(:,beta);
        % Conjugate Gradient Solver for the Poisson problem
        V1 = cgs(eLapl,-4*pi*tmp,1e-6,1000,[],[],x1);
        %V1 = gmres(eLapl,-4*pi*tmp, [], 1e-6, 1000);%,[],[], tmp);

        MVee(alpha,beta) = (conj(phi_e1(:,alpha)).*phi_e1(:,beta)).'*V1*de; 
       
        %Slater determinant mixed mixed-index densities 
        tmp = conj(phi_e2(:,alpha)).*phi_e1(:,beta);
        V2 = cgs(eLapl,-4*pi*tmp,1e-6,1000,[],[],x2);

        MVee(alpha,beta) = MVee(alpha,beta) - (conj(phi_e1(:,alpha)).*phi_e2(:,beta)).'*V2*de;
       
        if(alpha ~= beta) 
            tmp = conj(phi_e1(:,alpha)).*phi_e2(:,beta);
            V3 = cgs(eLapl,-4*pi*tmp,1e-6,1000,[],[],x3);
         
            MVee(alpha,beta) = MVee(alpha,beta) - (conj(phi_e2(:,alpha)).*phi_e1(:,beta)).'*V3*de;
            
            tmp = conj(phi_e1(:,alpha)).*phi_e1(:,beta);
            V4 = cgs(eLapl,-4*pi*tmp,1e-6,1000,[],[],x4);
         
            MVee(alpha,beta) = MVee(alpha,beta) + (conj(phi_e2(:,alpha)).*phi_e2(:,beta)).'*V4*de;
            
            if(alpha==1 && beta==2)
                x3=V3;
                x4=V4;
            end

        else %TODO: assumes Real
            MVee(alpha,beta) = 2*MVee(alpha,beta);     
        end
        fprintf('Finished alpha: %d beta: %d\n',alpha,beta) 
        x1=V1;
        x2=V2;
        
    end
end

dMVee = diag(diag(MVee));

MVee = MVee - dMVee + MVee';
%MVee = 0.5*MVee;

H = MTe + MVeExt + MVee;

%save('CWFs/HSmall.txt','H','-ascii','-double'); 
