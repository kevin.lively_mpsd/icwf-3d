
fprintf('Setting Operators\n')

if(uniformBox==true)

    %Values for the finitie difference diagonals
    valsDiag = [-6*ones(eDim,1)];
    %valsDiag = [3*(nchoosek(4,2))*ones(eDim,1)];
    %And off diagonals
    vals = [ones(eDim,1)];
    %vals1 = [-1*nchoosek(4,1)*ones(eDim,1)];
    %vals2 = [nchoosek(4,0)*ones(eDim,1)];
    %Higher order off diagonals would come here, so far, simple three point
   
    %Operators are flattened with
    %   x axis being the fastest changing index
    %   y axis being the second fastest changing index
    %   z axis being the slowest changing index 
    eLapl = spdiags(valsDiag,0,eDim,eDim) ...
          + spdiags(vals, 1, eDim,eDim)     + spdiags(vals, -1, eDim,eDim) ...
          + spdiags(vals, eDimy, eDim,eDim)    + spdiags(vals, -eDimy, eDim,eDim) ...
          + spdiags(vals, eDimy*eDimz, eDim,eDim) + spdiags(vals, -eDimy*eDimz, eDim,eDim);
    %valsDiag = [3*(-1)^3 * nchoosek(6,3)*ones(eDim,1)];
    %vals1 = [(-1)^2 * nchoosek(6,2)*ones(eDim,1)];
    %vals2 = [(-1)^1 * nchoosek(6,1)*ones(eDim,1)];
    %vals3 = [(-1)^0 * nchoosek(6,0)*ones(eDim,1)];
    %eLapl = spdiags(valsDiag,0,eDim,eDim) ...
    %      + spdiags(vals1, 1, eDim,eDim)             + spdiags(vals1, -1, eDim,eDim) ...
    %      + spdiags(vals1, eDimy, eDim,eDim)         + spdiags(vals1, -eDimy, eDim,eDim) ...
    %      + spdiags(vals1, eDimy*eDimz, eDim,eDim)   + spdiags(vals1, -eDimy*eDimz, eDim,eDim) ...
    %      + spdiags(vals2, 2, eDim,eDim)             + spdiags(vals2, -2, eDim,eDim) ...
    %      + spdiags(vals2, 2*eDimy, eDim,eDim)       + spdiags(vals2, -2*eDimy, eDim,eDim) ...
    %      + spdiags(vals2, 2*eDimy*eDimz, eDim,eDim) + spdiags(vals2, -2*eDimy*eDimz, eDim,eDim) ...
    %      + spdiags(vals3, 3, eDim,eDim)             + spdiags(vals3, -3, eDim,eDim) ...
    %      + spdiags(vals3, 3*eDimy, eDim,eDim)       + spdiags(vals3, -3*eDimy, eDim,eDim) ...
    %      + spdiags(vals3, 3*eDimy*eDimz, eDim,eDim) + spdiags(vals3, -3*eDimy*eDimz, eDim,eDim);
    
    eLapl = (1/dxe^2)*eLapl; 
    Te = (-1/(2*mu_e))*eLapl;
    if(Nnuc > 0)
        %Values for the finitie difference diagonals
        %And off diagonals
        %Higher order off diagonals would come here, so far, simple three point
       
        %Operators are flattened with
        %   x axis being the fastest changing index
        %   y axis being the second fastest changing index
        %   z axis being the slowest changing index 
        valsDiag = [-2*ones(nDim,1)];
        vals = [ones(nDim,1)];
        nLapl = spdiags(valsDiag,0,nDim,nDim) ...
              + spdiags(vals, 1, nDim,nDim)     + spdiags(vals, -1, nDim,nDim);
        
        %valsDiag = [(-1)^3 * nchoosek(6,3)*ones(nDim,1)];
        %vals1 = [(-1)^2 * nchoosek(6,2)*ones(nDim,1)];
        %vals2 = [(-1)^1 * nchoosek(6,1)*ones(nDim,1)];
        %vals3 = [(-1)^0 * nchoosek(6,0)*ones(nDim,1)];
        %nLapl = spdiags(valsDiag,0,nDim,nDim) ...
        %      + spdiags(vals1, 1, nDim,nDim)     + spdiags(vals1, -1, nDim,nDim) ...
        %      + spdiags(vals2, 2, nDim,nDim)     + spdiags(vals2, -2, nDim,nDim) ...
        %      + spdiags(vals3, 3, nDim,nDim)     + spdiags(vals3, -3, nDim,nDim);
        
        nLapl = (1/dxn^2)*nLapl; 
        Tn = (-1/(2*mu_n))*nLapl;
    end
 
    %electronic position vector 
    re = zeros(eDim,3);
    boundaryIndex = [];
    for zi=1:eDimz
        for yi=1:eDimy
            m = (1:eDimz) + eDimy*(yi-1 + eDimz*(zi-1)); %flattened index
            re(m,1) = eAxisx(:);
            re(m,2) = eAxisy(yi);
            re(m,3) = eAxisz(zi);
            if(yi==1 || yi==eDimy || zi==1 || zi==eDimz)
                boundaryIndex = [boundaryIndex m];
            else
                boundaryIndex = [boundaryIndex m(1) m(end)];
            end
            %g(m,1) = gAxis(:); 
            %g(m,2) = gAxis(yi); 
            %g(m,3) = gAxis(zi); 
        end
    end
   
    Ve_na = zeros(eDim,NTraj);

    Vn_ea = zeros(nDim,NTraj);
    Vnn = 1./nAxis;
    Ven = zeros(nDim,eDim);
    for Ri=1:nDim
        Ven(Ri,:) = Z(1)./sqrt( (re(:,1) - (m1/(m1+m2))*nAxis(Ri)).^2 ...
              +          (re(:,2)).^2 ...
              +          (re(:,3)).^2 ...
              +          softEN   )   ...
              +        Z(1)./sqrt( (re(:,1) + (m2/(m1+m2))*nAxis(Ri)).^2 ...
              +          (re(:,2)).^2 ...
              +          (re(:,3)).^2 ...
              +          softEN   );
    end
    %Soft ee conditional potentials
    for Ni=1:NTraj
       Ve_na(:,Ni) = Z(1)./sqrt( (re(:,1) - (m1/(m1+m2))*rn(Ni)).^2 ...
              +          (re(:,2)).^2 ...
              +          (re(:,3)).^2 ...
              +          softEN   )   ...
              +        Z(1)./sqrt( (re(:,1) + (m2/(m1+m2))*rn(Ni)).^2 ...
              +          (re(:,2)).^2 ...
              +          (re(:,3)).^2 ...
              +          softEN   );
       Vn_ea(:,Ni) = Z(1)./sqrt( (re1(Ni,1) - (m1/(m1+m2))*nAxis).^2 ...
              +          (re1(Ni,2)).^2 ...
              +          (re1(Ni,3)).^2  + softEN) ...
              +      Z(1)./sqrt( (re1(Ni,1) + (m2/(m1+m2))*nAxis).^2 ...
              +          (re1(Ni,2)).^2 ...
              +          (re1(Ni,3)).^2 + softEN);

    end
   
    %TODO: incoroporate external potentials here. 


end
