

redE = zeros(eDim,1);
for i=1:NTraj*M
for j=1:NTraj*M
    redE= redE + real( ...
                conj(C(i)).*conj(phi_e2(:,i)).*(Se1(i,j)*(C(j).*phi_e2(:,j))) ...
           +    conj(C(i)).*conj(phi_e1(:,i)).*(Se2(i,j)*(C(j).*phi_e1(:,j))) ...
           -  2*conj(C(i)).*conj(phi_e2(:,i)).*(Se1e2(i,j)*(C(j).*phi_e1(:,j))));
end
end

save('redEGs.txt','redE','-ascii','-double')
