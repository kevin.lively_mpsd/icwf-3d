

rxTrajs = repmat(re(:,1),1,NTraj*M);
ryTrajs = repmat(re(:,2),1,NTraj*M);
rzTrajs = repmat(re(:,3),1,NTraj*M);
RTrajs = repmat(nAxis,1,NTraj*M);

dOp1 =  -1*Sn.*(phi_e1'*(rxTrajs.*phi_e1)*de);
dOp2 =  -1*Sn.*(phi_e1'*(ryTrajs.*phi_e1)*de);
dOp3 =  -1*Sn.*(phi_e1'*(rzTrajs.*phi_e1)*de);

kickOp1 = 1e-3*dOp1;
kickOp2 = 1e-3*dOp2;
kickOp3 = 1e-3*dOp3;

ROp = (Se1.*(phi_n'*(RTrajs.*phi_n)*dxn));

propC = -1i*Sinv*H; %propC2 = -1i*Sinv*dOp;
saveInd=1;

COrig = -1j*Sinv*(H + 0*kickOp1)*C_ini*dt + C_ini; COrig = COrig./sqrt(COrig'*S*COrig); 
C = COrig; 
t=1;

fileNameD = strcat('dipole',num2str(0)); 
fileNameN = strcat('redN',num2str(0)); 
fileNameR = strcat('RPos',num2str(0)); 
fileNameE = strcat('Energy',num2str(0)); 
dipole = zeros(max(size(saveTime)),4); dipole(:,1) = saveTime;
RPos = zeros(max(size(saveTime)),1);
%redN = zeros(nDim,max(size(saveTime))+1); redN(:,1) = nAxis;
Energy = zeros(max(size(saveTime)),2); Energy(:,1) = saveTime;
for t=1:max(size(time))
    if(mod(time(t),saveTime(saveInd))==0)
        %saveRDE
        dipole(saveInd,2) = gather(real(C'*dOp1*C));
        dipole(saveInd,3) = gather(real(C'*dOp2*C));
        dipole(saveInd,4) = gather(real(C'*dOp3*C));
        Energy(saveInd,2) = gather(real(C'*H*C));
        RPos(saveInd) = gather(real(C'*ROp*C));
        %redE = zeros(eDimx,2);
        %for i = 1:NTraj*M
        %    redN(:,saveInd+1) = redN(:,2) + C(i)*((conj(phi_n).*repmat(phi_n(:,i),1,NTraj*M)))*(conj(C).*Se1(:,i).*Se2(:,i));
           %red_e = red_e + C(i)*((conj(phi_e1).*repmat(phi_e1(:,i),1,NTraj*M)))*(conj(C).*Se2(:,i).*Sn(:,i));
        %end
     
        if(mod(saveTime(saveInd),100)==0)
            save(strcat(savePath,fileNameD,savePathEnd,'.txt'), 'dipole', '-ascii','-double')
            %save(strcat(savePath,fileNameN,savePathEnd,'.txt'), 'redN', '-ascii','-double')
            save(strcat(savePath,fileNameR,savePathEnd,'.txt'), 'RPos', '-ascii','-double')
            save(strcat(savePath,fileNameE,savePathEnd,'.txt'), 'Energy', '-ascii','-double')
        end
        saveInd = saveInd + 1;
    end
    runge_kutta_prop_Cs 
    t = t+1;
end
save(strcat(savePath,fileNameD,savePathEnd,'.txt'), 'dipole', '-ascii','-double')
%save(strcat(savePath,fileNameN,savePathEnd,'.txt'), 'redN', '-ascii','-double')
save(strcat(savePath,fileNameR,savePathEnd,'.txt'), 'RPos', '-ascii','-double')
save(strcat(savePath,fileNameE,savePathEnd,'.txt'), 'Energy', '-ascii','-double')

