
mi=1;rng(mi);
%Ne = 1;
%M=3;
nPeriods = 5;
I = 5e13; %W/cm^2

newCWF=false;
if(newCWF==false)
    saveCWFs=true;
end

lowdinDrivenBoot

loadUniformOp
%loadLowdinMatrixElements
setLowdinMatrixElements

Imaginary_time_propCs


GPU=true;
if(GPU==true)
    Sinv = gpuArray(Sinv);
    HGPU = gpuArray(H);
    C_ini = gpuArray(C_ini);
    dOp1GPU = gpuArray(dOp1);
        propC = -1i*Sinv*HGPU;
        propC2 = -1i*Sinv*dOp1GPU;
end
C = C_ini;
t=1;
for t=1:10
tic

            runge_kutta_prop_FieldCs
toc
end
fprintf('Now CPU\n')

    Sinv = gather(Sinv);
    H = gather(H);
    C_ini = gather(C_ini);
    dOp1GPU = gather(dOp1);
    propC = -1i*Sinv*H;
            propC2 = -1i*Sinv*dOp1;
C = C_ini;
for t=1:10
tic

            runge_kutta_prop_FieldCs
toc
end
