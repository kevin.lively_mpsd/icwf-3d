if(true)
MVnn = load(strcat(savePath,'MVnn',savePathEnd,'.txt')); %Full
MTn  = load(strcat(savePath,'MTn',savePathEnd,'.txt')); %Full
MVen = load(strcat(savePath,'MVen',savePathEnd,'.txt')); %Upper Triangular
MTe1 = load(strcat(savePath,'MTe1',savePathEnd,'.txt')); %Upper Triangular
%Se1  = load(strcat(savePath,'Se1',savePathEnd,'.txt')); %Full 
Sn   = load(strcat(savePath,'Sn',savePathEnd,'.txt')); %Full
dOp1   = load(strcat(savePath,'dOp1',savePathEnd,'.txt')); %Upper Triangular
dOp2   = load(strcat(savePath,'dOp2',savePathEnd,'.txt')); %Upper Triangular
dOp3   = load(strcat(savePath,'dOp3',savePathEnd,'.txt')); %Upper Triangular
phi_n   = load(strcat(savePath,'phiNUniDist',savePathEnd,'.txt')); 

ROp = (Se1).*(phi_n(:,2:end)'*(repmat(nAxis,1,(size(phi_n,2)-1)).*phi_n(:,2:end))*dxn);
NRs = max(size(Rsample));
redNOp = zeros(nDim,2*Ne*M*NRs,2*Ne*M*NRs);
for j=1:2*Ne*M*NRs
    redNOp(:,j,:) = phi_n(:,2:end).*(conj(phi_n(:,j+1))*Se1(j,:));
    %for j=1:2*Ne*M*NRs
    %    redNOp(:,j,i) = conj(phi_n(:,j)).*phi_n(:,i)*Se1(j,i);
    %end
end

dMVen = diag(diag(MVen));
dMTe1 = diag(diag(MTe1));
ddOp1 = diag(diag(dOp1));
ddOp2 = diag(diag(dOp2));
ddOp3 = diag(diag(dOp3));

MVen = MVen - dMVen + MVen';
MTe1 = MTe1 - dMTe1 + MTe1';
dOp1 = Sn.*(dOp1 - ddOp1 + dOp1');
dOp2 = Sn.*(dOp2 - ddOp2 + dOp2');
dOp3 = Sn.*(dOp3 - ddOp3 + dOp3');

S = Se1.*Sn;
%MVnn = triu(MVnn) - diag(diag(MVnn)) + triu(MVnn)';
%MTn = triu(MTn) - diag(diag(MTn)) + triu(MTn)';

U = MVen + Se1.*MVnn;
H = Se1.*MTn + Sn.*MTe1 + U;

HOrig = H;
SOrig = S;
MVenOrig = MVen;
MTe1Orig = MTe1;
dOp1Orig = dOp1;
dOp2Orig = dOp2;
dOp3Orig = dOp3;
ROpOrig = ROp;
redNOpOrig = redNOp;
re = zeros(eDim,3);
boundaryIndex = [];
for zi=1:eDimz
    for yi=1:eDimy
        m = (1:eDimz) + eDimy*(yi-1 + eDimz*(zi-1)); %flattened index
        re(m,1) = eAxisx(:);
        re(m,2) = eAxisy(yi);
        re(m,3) = eAxisz(zi);
        if(yi==1 || yi==eDimy || zi==1 || zi==eDimz)
            boundaryIndex = [boundaryIndex m];
        else
            boundaryIndex = [boundaryIndex m(1) m(end)];
        end
        %g(m,1) = gAxis(:); 
        %g(m,2) = gAxis(yi); 
        %g(m,3) = gAxis(zi); 
    end
end
else
    H = HOrig;
    S = SOrig;
    MVen = MVenOrig;
    MTe1 = MTe1Orig;
    dOp1 = dOp1Orig;
    dOp2 = dOp2Orig;
    dOp3 = dOp3Orig;
    ROp = ROpOrig;
    redNOp = redNOpOrig;
end
SnTmp = Sn;
SnTmp(log10(abs(Sn))<-8)=0;
Se1Tmp = Se1;
Se1Tmp(log10(abs(Se1))<-8)=0;
HTmp = H;
HTmp(log10(abs(H))<-8)=0;
%H = HTmp;

%S = Se1Tmp.*SnTmp;

subsGs = [(1:2*M*Ne*subSkip:2*M*Ne*NRs)];% 2:2*M*Ne*subSkip:2*M*Ne*NRs 3:2*M*Ne*subSkip:2*M*Ne*NRs]; 
subsEx = [((Ne*M+1):2*M*Ne*subSkip:2*M*Ne*NRs)];
subs = [1:M*Ne*subSkip:200*2*M*Ne  ];
subs = [subs (M*Ne+1):M*Ne*subSkip:200*2*M*Ne  ];
subs = unique(subs);
%subs = unique(randi(2*Ne*M*100,400,1)); 
%subs = sort(subs);

%p = [[eye(max(size(subsGs))) zeros(max(size(subsGs)))];[zeros(max(size(subsGs))) zeros(max(size(subsGs)))]];
Sinv = pinv(S(subs,subs),tolerance);
[Q,Sig] = eig(S(subs,subs));
SigInv = zeros(max(size(Sig)));
for i=1:max(size(Sig))
    %SigInv(i,i) = 1./(Sig(i,i) + tolerance);
    if(log10(abs(Sig(i,i))) < log10(tolerance))
        SigInv(i,i) = 0;
    else
        SigInv(i,i) = 1./Sig(i,i);
    end
end
Sinv2 = Q*SigInv*Q';

[C,E] = eigs(Sinv*H(subs,subs),1,'sr'); 
%C((end+1):2*max(size(C))) = 0; 
C = C./sqrt(C'*S(subs,subs)*C);


%C(end+1:end+max(size((M*Ne+1):M*Ne*subSkip:200*2*M*Ne))) = 0;

C_ini = C;
H = H(subs,subs);
S = S(subs,subs);
[L,U] = ilu(sparse(S),struct('type','ilutp','droptol',1e-8));
%setup = struct('type','ict','diagcomp',1e-6,'droptol',1e-14);
%L2 = ichol(sparse(S),setup);
%Sinv = pinv(S,tolerance);
MVen = MVen(subs,subs);
MTe1 = MTe1(subs,subs);
dOp1 = dOp1(subs,subs);
dOp2 = dOp2(subs,subs);
dOp3 = dOp3(subs,subs);
ROp = ROp(subs,subs);
redNOp = redNOp(:,subs,subs);
