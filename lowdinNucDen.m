function redN = lowdinNucDen(redNOp,Se1,C)

    nDim = size(redNOp,1);
    Ne = size(Se1,2);
    Nn = size(redNOp,2);
    
    redN = zeros(nDim,1);

    for iek=1:Ne
        for iel = 1:Ne
            iglobal = ((1:Nn) + Nn*(iek-1)).';
            for j=1:Nn
                jglobal = j + Nn*(iel-1);
                redN = redN + Se1(iek,iel)*conj(C(jglobal))*(squeeze(redNOp(:,j,:))*C(iglobal));
            end
        end
    end
end
