mi=1;rng(mi);
NTraj = 32;
M=2;
nPeriods = 5;
I = 5e13; %W/cm^2

newCWF=false;
if(newCWF==false)
    saveCWFs=true;
end

drivenBoot

%loadBO

if(newCWF==true)
    sampleTrajExact
    
    setOperators
    
    setCWF
    
    setMatrixElements

    Imaginary_time_propCs
else
    loadCWF
    
    loadMatrixElements
    
    setOperators
end

H_CsDriven
