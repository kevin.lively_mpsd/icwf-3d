BOBoot

valsDiag = [-6*ones(eDim,1)];
vals = [ones(eDim,1)];
eLapl = spdiags(valsDiag,0,eDim,eDim) ...
      + spdiags(vals, 1, eDim,eDim)     + spdiags(vals, -1, eDim,eDim) ...
      + spdiags(vals, eDimy, eDim,eDim)    + spdiags(vals, -eDimy, eDim,eDim) ...
      + spdiags(vals, eDimy*eDimz, eDim,eDim) + spdiags(vals, -eDimy*eDimz, eDim,eDim);

eLapl = (1/dxe^2)*eLapl; 
Te = (-1/(2*mu_e))*eLapl;
    
re = zeros(eDim,3);
boundaryIndex = [];
for zi=1:eDimz
    for yi=1:eDimy
        m = (1:eDimz) + eDimy*(yi-1 + eDimz*(zi-1)); %flattened index
        re(m,1) = eAxisx(:);
        re(m,2) = eAxisy(yi);
        re(m,3) = eAxisz(zi);
    end
end


BOstates = zeros(nDim,eDim,ex);
BOenergy = zeros(nDim,ex+1); BOenergy(:,1) = nAxis;
nGPU = gpuDeviceCount();
matlabpool local nGPU
parfor Ri=1:nDim
    Ven = Z(1)./sqrt( (re(:,1) - (m1/(m1+m2))*nAxis(Ri)).^2 ...
          +          (re(:,2)).^2 ...
          +          (re(:,3)).^2 ...
          +          softEN   )   ...
          +        Z(1)./sqrt( (re(:,1) + (m2/(m1+m2))*nAxis(Ri)).^2 ...
          +          (re(:,2)).^2 ...
          +          (re(:,3)).^2 ...
          +          softEN   );

    [tmpS, tmpE] = eigs(Te + spdiags(Ven,0,eDim,eDim),ex,'sa');
    if(mean(tmpS(:,1)<0))
        tmpS(:,1) = -1*tmpS(:,1);
    end

    for i = 1:ex
        tmpS(:,i) = tmpS(:,i)./sqrt(tmpS(:,i)'*tmpS(:,i)*de);
    end

    BOstates(Ri,:,:) = tmpS;
    BOenergy(Ri,2:end) = diag(tmpE) + 1/nAxis(Ri);

    fprintf('%d percent done\n', 100*Ri/nDim)
end
save(strcat(savePath,'BOenergy.txt'),'BOenergy','-ascii','-double')

limit = 0.2;
for R_aux = 1:nDim
    for i=1:ex
        aux = sum(squeeze(BOstates(R_aux,:,i)).*squeeze(BOstates(R_aux,:,i)));
        if aux < limit; BOstates(R_aux,:,i) = -real(BOstates(R_aux,:,i)); end
    end
end

save(strcat(savePath,'BOstates'),'BOstates')
