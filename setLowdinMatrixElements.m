
if(true)

if(interaction==true)
    phiCan = load('VenEle.mat');
    phi_e = phiCan.V;
    NeFull = size(phiCan.V,2);
else
    phiCan = load(strcat('BO_dxe_',num2str(dxe),'_boxL_',num2str(boxL),'_dx_n_',num2str(dxn),'_boxLn_',num2str(boxLn),'_softEN_',num2str(softEN),'/BOCan.mat'));
    phi_e = phiCan.phiCan;
    NeFull = size(phiCan.phiCan,2);    
end
clear phiCan;


loadBO

if(Ne>size(phi_e,2))
    Ne = size(phi_e,2);
else
    phi_e = phi_e(:,1:Ne);
end

if(planeWave==true)
    [eigV,eigE] = eig(full(Tn));
    for i=1:nDim
        eigV(:,i) = eigV(:,i)./sqrt(eigV(:,i)'*eigV(:,i)*dxn);
    end
    phi_n = eigV;
elseif(canonical==true)
    phi_n = load(strcat(savePath,'chiCan.txt'));
elseif(interaction==true)
    phi_n = load(strcat(savePath,'interactionNucBasis.txt'));
end

if(Nn<=size(phi_n,2))
    phi_n = phi_n(:,1:Nn);
else
    Nn = size(phi_n,2);
end
redNOp = zeros(nDim,Nn,Nn);
for j=1:Nn
    for i=1:Nn
        redNOp(:,j,i) = conj(phi_n(:,j)).*phi_n(:,i);
    end
end

end %DEBUG END
savePathEnd = strcat('Ne_',num2str(Ne),'_Nn_',num2str(Nn))
NeflatFull = [];
%NeFull=30; %%%DEBUG
for i=1:NeFull
        for k=i:NeFull
                NeflatFull = [NeflatFull i + NeFull*(k-1) ];
        end
end

NucOp = load(strcat(savePath,'NucOp.txt'));
MVen = zeros(Ne*Nn);

index = 0; Ntotal = Ne*(Ne-1)/2 + Ne;
for iek=1:Ne
    for iel=iek:Ne
        flatIndex = iek+NeFull*(iel-1);
        [dum, Ef] = min(abs(flatIndex - NeflatFull)); 
        for inuc=1:Nn
            MVen(inuc + Nn*(iek-1), (1:Nn) + Nn*(iel-1)) = (repmat(conj(phi_n(:,inuc)),1,Nn).*phi_n)'*NucOp(:,Ef)*dxn*de;
        end
        index = index + 1;
        if(mod(index,10)==0)
            fprintf('%f percent done\n', 100*index/Ntotal );
        %    save(strcat(savePath,'LowdinMVen.txt'),'MVen','-ascii','-double');
        end
    end
end
MVen = MVen - tril(MVen) + triu(MVen).';
%save(strcat(savePath,'LowdinMVen.txt'),'MVen','-ascii','-double');

%MVen = load(strcat(savePath,'LowdinMVen.txt'),'MVen');


Se1 = phi_e'*phi_e*de;
Se1 = Se1 - tril(Se1) + triu(Se1).';
%Se1(abs(Se1)<1e-12)=0;
Sn = phi_n'*phi_n*dxn;
Sn = Sn - tril(Sn) + triu(Sn).';
%Sn(abs(Sn)<1e-12)=0;
S = kron(Se1,Sn);

STn = phi_n'*(Tn*phi_n)*dxn;
STn = STn - tril(STn) + triu(STn).';
STe1 = phi_e'*(Te*phi_e)*de;
STe1 = STe1 - tril(STe1) + triu(STe1).';
MTn = kron(Se1, STn);
MTe1 = kron(STe1,Sn);

SVnn = phi_n'*(repmat(1./nAxis,1,Nn).*phi_n)*dxn;
SVnn = SVnn - tril(SVnn) + triu(SVnn).';
MVnn = kron(Se1,SVnn);

H = MTn + MTe1 + MVnn + MVen;

%save(strcat(savePath,'LowdinH.txt'),'H','-ascii','-double');
%save(strcat(savePath,'LowdinS.txt'),'S','-ascii','-double');

rxTrajs = repmat(re(:,1),1,Ne);
ryTrajs = repmat(re(:,2),1,Ne);
rzTrajs = repmat(re(:,3),1,Ne);
sOp1 = phi_e'*(rxTrajs.*phi_e)*de;
sOp1 = sOp1 - tril(sOp1) + triu(sOp1).';
sOp2 = phi_e'*(ryTrajs.*phi_e)*de;
sOp2 = sOp2 - tril(sOp2) + triu(sOp2).';
sOp3 = phi_e'*(rzTrajs.*phi_e)*de;
sOp3 = sOp3 - tril(sOp3) + triu(sOp3).';
dOp1 =  -1*kron(sOp1,Sn);
dOp2 =  -1*kron(sOp2,Sn);
dOp3 =  -1*kron(sOp3,Sn);

RTrajs = repmat(nAxis,1,Nn);
sROp = phi_n'*(RTrajs.*phi_n)*dxn;
sROp = sROp - tril(sROp) + triu(sROp).';
ROp = kron(Se1,sROp);

CAPIndex = [];
boundaryIndex = [];
eta = 0.1;
UCap = 0j*zeros(size(re,1),1);
cutoffInd = 10;
for zi=1:eDimz
    for yi=1:eDimy
        m = (1:eDimz) + eDimy*(yi-1 + eDimz*(zi-1)); %flattened index

        %Start by assigning X, then reassign if on y or z or plane boundary
        UCap(m(1:cutoffInd))         = -1j*eta*( (eAxisx(1:cutoffInd) - eAxisx(cutoffInd)).^2) ;
        UCap(m(end-cutoffInd+1:end)) = -1j*eta*( (eAxisx(end-cutoffInd+1:end) - eAxisx(end-cutoffInd+1)).^2) ;
        if(true)
        % In Y Boundaries
        if( (yi <= cutoffInd) && (zi > cutoffInd && zi < eDimz - cutoffInd) )
            
            %X and Y Wall Meeting
            UCap(m(1:cutoffInd))               = -1j*eta*( (eAxisx(1:cutoffInd) - eAxisx(cutoffInd)).^2 ...
                                                         + (eAxisy(yi) - eAxisy(cutoffInd)).^2 );            
            UCap(m(end-cutoffInd+1:end))       = -1j*eta*( (eAxisx(end-cutoffInd+1:end) - eAxisx(end-cutoffInd+1)).^2 ...
                                                         + (eAxisy(yi) - eAxisy(cutoffInd)).^2 );            
            %Y Region
            UCap(m(cutoffInd+1:end-cutoffInd)) = -1j*eta*( (eAxisy(yi) - eAxisy(cutoffInd)).^2 );            
        end
        if( (yi >= eDimy-cutoffInd+1) && (zi > cutoffInd && zi < eDimz - cutoffInd) )
            %X and Y Wall Meeting
            UCap(m(1:cutoffInd))               = -1j*eta*( ( eAxisx(1:cutoffInd) - eAxisx(cutoffInd) ).^2 ...
                                                         + ( eAxisy(yi) - eAxisy(end - cutoffInd+1) ).^2 );            
            UCap(m(end-cutoffInd+1:end))       = -1j*eta*( (eAxisx(end-cutoffInd+1:end) - eAxisx(end-cutoffInd+1)).^2 ...
                                                         + ( eAxisy(yi) - eAxisy(end - cutoffInd+1) ).^2 );            
            %Y Region
            UCap(m(cutoffInd+1:end-cutoffInd)) = -1j*eta*( (eAxisy(yi) - eAxisy(end-cutoffInd+1)).^2 );            
        end
        % Z Only
        if( (zi <= cutoffInd) && (yi > cutoffInd && yi < eDimy - cutoffInd) )
            %X and Z Wall Meeting
            UCap(m(1:cutoffInd))         = -1j*eta*( (eAxisx(1:cutoffInd) - eAxisx(cutoffInd)).^2 ...
                                                   + (eAxisz(zi) - eAxisz(cutoffInd)).^2 );            
            UCap(m(end-cutoffInd+1:end)) = -1j*eta*( (eAxisx(end-cutoffInd+1:end) - eAxisx(end-cutoffInd+1)).^2 ...
                                                   + (eAxisz(zi) - eAxisz(cutoffInd)).^2 );            
            %Z Region
            UCap(m(cutoffInd+1:end-cutoffInd)) = -1j*eta*( (eAxisz(zi) - eAxisz(cutoffInd)).^2 );            
        end
        if( (zi >= eDimy-cutoffInd+1) && (yi > cutoffInd && yi < eDimz - cutoffInd) )
            %X and Z Wall Meeting
            UCap(m(1:cutoffInd))         = -1j*eta*( (eAxisx(1:cutoffInd) - eAxisx(cutoffInd) ).^2 ...
                                                   + (eAxisz(zi) - eAxisz(end - cutoffInd+1) ).^2 );            
            UCap(m(end-cutoffInd+1:end)) = -1j*eta*( (eAxisx(end-cutoffInd+1:end) - eAxisx(end-cutoffInd+1)).^2 ...
                                                   + (eAxisz(zi) - eAxisz(end - cutoffInd+1) ).^2 );            
            %Z Region
            UCap(m(cutoffInd+1:end-cutoffInd)) = -1j*eta*( (eAxisz(zi) - eAxisz(end-cutoffInd+1)).^2 );            
        end
        %YZ Boundary
        if( (zi <= cutoffInd) && (yi <= cutoffInd) )
            %XYZ corners
            UCap(m(1:cutoffInd))         = -1j*eta*( (eAxisx(1:cutoffInd) - eAxisx(cutoffInd)).^2 ...
                                                   + (eAxisy(yi) - eAxisy(cutoffInd)).^2  ...          
                                                   + (eAxisz(zi) - eAxisz(cutoffInd)).^2 );            
            UCap(m(end-cutoffInd+1:end)) = -1j*eta*( (eAxisx(end-cutoffInd+1:end) - eAxisx(end-cutoffInd+1)).^2 ...
                                                   + (eAxisy(yi) - eAxisy(cutoffInd)).^2  ...          
                                                   + (eAxisz(zi) - eAxisz(cutoffInd)).^2 );            
           
            %Y and Z Wall Meeting 
            UCap(m(cutoffInd+1:end-cutoffInd)) = -1j*eta*( (eAxisy(yi) - eAxisy(cutoffInd)).^2  ...          
                                                         + (eAxisz(zi) - eAxisz(cutoffInd)).^2 );            
        end
        if( (zi >= eDimz-cutoffInd +1) && (yi <= cutoffInd) )
            %XYZ corners
            UCap(m(1:cutoffInd))         = -1j*eta*( (eAxisx(1:cutoffInd) - eAxisx(cutoffInd) ).^2 ...
                                                   + (eAxisy(yi) - eAxisy(cutoffInd)).^2  ...          
                                                   + (eAxisz(zi) - eAxisz(eDimz - cutoffInd+1) ).^2 );            
            UCap(m(end-cutoffInd+1:end)) = -1j*eta*( (eAxisx(end-cutoffInd+1:end) - eAxisx(end-cutoffInd+1)).^2 ...
                                                   + (eAxisy(yi) - eAxisy(cutoffInd)).^2  ...          
                                                   + (eAxisz(zi) - eAxisz(eDimz - cutoffInd+1) ).^2 );            
           
            %Y and Z Wall Meeting 
            UCap(m(cutoffInd+1:end-cutoffInd)) = -1j*eta*( (eAxisy(yi) - eAxisy(cutoffInd)).^2  ...          
                                                         + (eAxisz(zi) - eAxisz(eDimz - cutoffInd+1) ).^2 );            
        end
        if( (zi <= cutoffInd) && (yi >= eDimy - cutoffInd +1) )
            %XYZ corners
            UCap(m(1:cutoffInd))         = -1j*eta*( (eAxisx(1:cutoffInd) - eAxisx(cutoffInd)).^2 ...
                                                   + (eAxisy(yi) - eAxisy(eDimy - cutoffInd+1) ).^2  ...            
                                                   + (eAxisz(zi) - eAxisz(cutoffInd)).^2 );            
            UCap(m(end-cutoffInd+1:end)) = -1j*eta*( (eAxisx(end-cutoffInd+1:end) - eAxisx(end-cutoffInd+1)).^2 ...
                                                   + (eAxisy(yi) - eAxisy(eDimy - cutoffInd+1) ).^2  ...            
                                                   + (eAxisz(zi) - eAxisz(cutoffInd)).^2 );            
           
            %Y and Z Wall Meeting 
            UCap(m(cutoffInd+1:end-cutoffInd)) = -1j*eta*( (eAxisy(yi) - eAxisy(eDimy - cutoffInd+1) ).^2  ...          
                                                         + (eAxisz(zi) - eAxisz(cutoffInd)).^2 );            
        end
        if( (zi >= eDimz-cutoffInd + 1) && (yi >= eDimy - cutoffInd + 1) )
            %XYZ corners
            UCap(m(1:cutoffInd))         = -1j*eta*( (eAxisx(1:cutoffInd) - eAxisx(cutoffInd) ).^2 ...
                                                   + (eAxisy(yi) - eAxisy(eDimy - cutoffInd+1) ).^2  ...            
                                                   + (eAxisz(zi) - eAxisz(eDimz - cutoffInd+1) ).^2 );            
            UCap(m(end-cutoffInd+1:end)) = -1j*eta*( (eAxisx(end-cutoffInd+1:end) - eAxisx(end-cutoffInd+1)).^2 ...
                                                   + (eAxisy(yi) - eAxisy(eDimy - cutoffInd+1) ).^2  ...            
                                                   + (eAxisz(zi) - eAxisz(eDimz - cutoffInd+1) ).^2 );            
           
            %Y and Z Wall Meeting 
            UCap(m(cutoffInd+1:end-cutoffInd)) = -1j*eta*( (eAxisy(yi) - eAxisy(eDimy - cutoffInd+1) ).^2  ...            
                                                         + (eAxisz(zi) - eAxisz(eDimz - cutoffInd+1) ).^2 );            
        end
        
        end %DEBUG2
        if(yi <= cutoffInd || yi >= eDimy-cutoffInd || zi <= cutoffInd || zi >= eDimz-cutoffInd)
            CAPIndex = [CAPIndex m];
        else
            CAPIndex = [CAPIndex m(1:cutoffInd) m(end-cutoffInd+1:end)];
            
        end
        %g(m,1) = gAxis(:); 
        %g(m,2) = gAxis(yi); 
        %g(m,3) = gAxis(zi); 
    end
end


SUe = phi_e'*(repmat(UCap,1,Ne).*phi_e)*de;
MU = kron(SUe,Sn);


Wn=zeros(size(nAxis));

L_ind = 100;
Wn(end-L_ind:end) = -1i*eta*(nAxis(end-L_ind:end) - nAxis(end-L_ind)).^2;

SUn = phi_n'*(repmat(Wn,1,Nn).*phi_n)*dxn;
MUn = kron(Se1,SUn);

H = H + MUn + MU;

loadBO
Fg = BOgs'*phi_e*de;
Fe = BOex'*phi_e*de;


%save(strcat(savePath,'LowdindOp1.txt'),'dOp1','-ascii','-double');
%save(strcat(savePath,'LowdindOp2.txt'),'dOp2','-ascii','-double');
%save(strcat(savePath,'LowdindOp3.txt'),'dOp3','-ascii','-double');

%save(strcat(savePath,'LowdinROp.txt'),'ROp','-ascii','-double');
