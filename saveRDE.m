

Se1 = phi_e1'*phi_e1*de; Se2 = phi_e2'*phi_e2*de; Sn = phi_n'*phi_n*dxn;

redN = zeros(nDim,2); redN(:,1) = nAxis;
%redE = zeros(eDimx,2);
for i = 1:NTraj*M
    redN(:,2) = redN(:,2) + C(i)*((conj(phi_n).*repmat(phi_n(:,i),1,NTraj*M)))*(conj(C).*Se1(:,i).*Se2(:,i));
    %red_e = red_e + C(i)*((conj(phi_e1).*repmat(phi_e1(:,i),1,NTraj*M)))*(conj(C).*Se2(:,i).*Sn(:,i));
end
save(strcat(savePath,'redN.txt')','redN','-ascii','-double')
