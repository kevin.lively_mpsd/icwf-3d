
%Set grid Parameters, pseudoinverse tolerance, softening parameters for CWFs, etc
params

%Sample the initial particle positions
sampleTraj

%Set up the Kinetic energy operators and the CWF potentials 
setOperators

%Solve for the CWFs
setCWF

%load solved for CWFs
%loadCWF

%Calculate the Hamiltonian Matrix Elements
setMatrixElements

%Solve for the ansatz ground state
Imaginary_time_propCs

%Propagate
%H_Cs
