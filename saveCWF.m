
redVx = zeros(eDimx,NTraj*M+1);redVx(:,1) = eAxisx;
redVxy = zeros(eDimx*eDimy,NTraj*M);
sliceRedVxy = zeros(eDimx*eDimy,NTraj*M);
redVy = zeros(eDimy,NTraj*M+1);redVy(:,1) = eAxisy;
redVz = zeros(eDimz,NTraj*M+1);redVz(:,1) = eAxisz;
eSlicex2 = zeros(eDimx,NTraj*M+1);eSlicex(:,1) = eAxisx;
eSlicey = zeros(eDimy,NTraj*M+1);eSlicey(:,1) = eAxisy;
eSlicez = zeros(eDimz,NTraj*M+1);eSlicez(:,1) = eAxisz;

redVx2 = zeros(eDimx,NTraj*M+1);redVx2(:,1) = eAxisx;
redVxy2 = zeros(eDimx*eDimy,NTraj*M);
sliceRedVxy2 = zeros(eDimx*eDimy,NTraj*M);
redVy2 = zeros(eDimy,NTraj*M+1);redVy2(:,1) = eAxisy;
redVz2 = zeros(eDimz,NTraj*M+1);redVz2(:,1) = eAxisz;
eSlicex2 = zeros(eDimx,NTraj*M+1);eSlicex2(:,1) = eAxisx;
eSlicey2 = zeros(eDimy,NTraj*M+1);eSlicey2(:,1) = eAxisy;
eSlicez2 = zeros(eDimz,NTraj*M+1);eSlicez2(:,1) = eAxisz;
for i=1:NTraj*M
    tmp = reshape(abs(phi_e1(:,i)).^2,eDimx,eDimy,eDimz);
    redVx(:,i+1) = sum(sum(tmp,3),2)*dxe*dxe;
    redVy(:,i+1) = squeeze(sum(sum(tmp,3),1))*dxe*dxe;
    redVz(:,i+1) = squeeze(sum(sum(tmp,2),1))*dxe*dxe;
    
    tmp2 = sum(tmp,3)*dxe;
    redVxy(:,i) = tmp2(:);
    tmp3 = tmp(:,:,floor(eDimz/2));
    sliceRedVxy(:,i) = tmp3(:);

    tmp = reshape(phi_e1(:,i),eDimx,eDimy,eDimz);
    eSlicex(:,i +1) = squeeze(tmp(:,floor(eDimy/2),floor(eDimz/2)));
    eSlicey(:,i+1) = squeeze(tmp(floor(eDimx/2),:,floor(eDimz/2)));
    eSlicez(:,i+1) = squeeze(tmp(floor(eDimx/2),floor(eDimy/2),:));
    
    tmp = reshape(abs(phi_e2(:,i)).^2,eDimx,eDimy,eDimz);
    redVx2(:,i+1) = sum(sum(tmp,3),2)*dxe*dxe;
    redVy2(:,i+1) = squeeze(sum(sum(tmp,3),1))*dxe*dxe;
    redVz2(:,i+1) = squeeze(sum(sum(tmp,2),1))*dxe*dxe;
    
    tmp2 = sum(tmp,3)*dxe;
    redVxy2(:,i) = tmp2(:);
    tmp3 = tmp(:,:,floor(eDimz/2));
    sliceRedVxy2(:,i) = tmp3(:);

    tmp = reshape(phi_e2(:,i),eDimx,eDimy,eDimz);
    eSlicex2(:,i +1) = squeeze(tmp(:,floor(eDimy/2),floor(eDimz/2)));
    eSlicey2(:,i+1) = squeeze(tmp(floor(eDimx/2),:,floor(eDimz/2)));
    eSlicez2(:,i+1) = squeeze(tmp(floor(eDimx/2),floor(eDimy/2),:));
end

save('CWFs/eSlicex.txt', 'eSlicex','-ascii','-double');
save('CWFs/eSlicey.txt', 'eSlicey','-ascii','-double');
save('CWFs/eSlicez.txt', 'eSlicez','-ascii','-double');

save('CWFs/redVx.txt','redVx','-ascii','-double');
save('CWFs/redVxy.txt','redVxy','-ascii','-double');
save('CWFs/sliceRedVxy.txt','sliceRedVxy','-ascii','-double');
save('CWFs/redVy.txt','redVy','-ascii','-double');
save('CWFs/redVz.txt','redVz','-ascii','-double');

save('CWFs/eSlicex2.txt', 'eSlicex2','-ascii','-double');
save('CWFs/eSlicey2.txt', 'eSlicey2','-ascii','-double');
save('CWFs/eSlicez2.txt', 'eSlicez2','-ascii','-double');



save('CWFs/redVx2.txt','redVx2','-ascii','-double');
save('CWFs/redVxy2.txt','redVxy2','-ascii','-double');
save('CWFs/sliceRedVxy2.txt','sliceRedVxy2','-ascii','-double');
save('CWFs/redVy2.txt','redVy2','-ascii','-double');
save('CWFs/redVz2.txt','redVz2','-ascii','-double');
