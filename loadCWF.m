
fprintf('Loading CWFs\n')

phi_e1     = load(strcat(savePath,'phi_e1',savePathEnd,'.mat'));
phi_e1     = phi_e1.phi_e1;
phi_e1_ini = phi_e1;

re1 = load(strcat(savePath,'re1',savePathEnd,'.txt'));

phi_n     = load(strcat(savePath,'phi_n',savePathEnd,'.txt'));
phi_n     = phi_n(:,2:end);
phi_n_ini = phi_n;

rn = load(strcat(savePath,'rn',savePathEnd,'.txt'));

