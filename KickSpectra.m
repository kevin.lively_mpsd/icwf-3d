function [signal] = KickSpectra(dipole,time,kappa,energy, dir)

    eta = -log10(1e-3)/time(end);
    mask = exp(-eta*time);
   
    tmp = mask.'.*(dipole(:,dir+1) - dipole(1,dir+1));
    signal = zeros(max(size(energy)),1);
    dt = time(2)-time(1);
    for Ei=1:max(size(energy))
        signal(Ei) =  sum(exp(-1j*energy(Ei)*time.').*tmp);
    end
    signal = (4*pi/(137*kappa))*imag(signal.*energy.'*dt);


